import React, { useRef, useState } from "react";
import "../Admin.css";
import {
  Button,
  Form,
  Input,
  Modal,
  Select,
  Table,
  Image,
  InputNumber,
} from "antd";
import { useEffect } from "react";
import { https } from "../../services/config";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { regex } from "../../Constant/regex";
import { LocalService } from "../../services/LocalService";
import {
  LockOutlined,
  MailOutlined,
  UserOutlined,
  PhoneOutlined,
  SmileOutlined,
  BookOutlined,
  CalendarOutlined,
  UploadOutlined,
} from "@ant-design/icons";
import Swal from "sweetalert2";
import { update } from "../../redux/userSlice";
import {
  confirmStudentAttend,
  createCourse,
  delCoursesManagement,
  delStudentAttended,
  delStudentPendingAttend,
  searchCoursesManagement,
  setCoursesManagement,
  setDynamic,
  setDynamicCourse,
  setListStudentAttended,
  setListStudentInAttend,
  setListStudentPendingAttend,
  updateCoursesManagement,
} from "../../redux/Admin/coursesManagementSlice";
import Upload from "antd/es/upload/Upload";

export default function CoursesManagementDesktop() {
  const toggleRef = useRef(null);
  const dispatch = useDispatch();
  const [dataSearch, setDataSearch] = useState([]);
  const dataSource = useSelector(
    (state) => state.coursesManagement.coursesManagement
  );

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isModalOpen2, setIsModalOpen2] = useState(false);
  const [isModalOpen3, setIsModalOpen3] = useState(false);
  const [isModalOpen4, setIsModalOpen4] = useState(false);
  const [show, setShow] = useState(false);
  const user = useSelector((state) => state.userSlice.userInfo);
  const dynamic = useSelector((state) => state.coursesManagement.dynamic);

  // lấy danh sách học viên chưa đăng ký khóa học
  const listStudentInAttend = useSelector(
    (state) => state.coursesManagement.listStudentInAttend
  );
  //  lấy danh sách học viên đã tham gia khóa học
  const listStudentAttended = useSelector(
    (state) => state.coursesManagement.listStudent
  );
  // lấy danh sách học viên chờ xác thực
  const listStudentPendingAttend = useSelector(
    (state) => state.coursesManagement.listStudentPendingAttend
  );
  const dynamicCourse = useSelector(
    (state) => state.coursesManagement.dynamicCourse
  );
  const [catalogCourses, setCatalogCourses] = useState([]);
  const userDynamic = useSelector((state) => state.userManagement.userDynamic);

  // Lấy danh mục khóa học
  useEffect(() => {
    https
      .get("api/QuanLyKhoaHoc/LayDanhMucKhoaHoc")
      .then((res) => {
        setCatalogCourses(res.data);
      })
      .catch((err) => {});
  }, []);

  // Lấy danh sách khóa học
  useEffect(() => {
    https
      .get("api/QuanLyKhoaHoc/LayDanhSachKhoaHoc")
      .then((res) => {
        dispatch(setCoursesManagement(res.data));
        setDataSearch(res.data);
      })
      .catch((err) => {});
  }, []);
  // bảng dữ liệu danh sách khóa học
  const columns = [
    {
      title: "Mã khóa học",
      dataIndex: "maKhoaHoc",
      key: "maKhoaHoc",
    },
    {
      title: "Tên khóa học",
      dataIndex: "tenKhoaHoc",
      key: "tenKhoaHoc",
    },
    {
      title: "Hình Ảnh",
      dataIndex: "hinhAnh",
      key: "hinhAnh",
      render: (item) => {
        return <img className="object-cover w-48 h-48"  src={item} alt="img" />;
      },
    },
    {
      title: "Lượt xem",
      dataIndex: "luotXem",
      key: "luotXem",
    },
    {
      title: "Người Tạo",
      dataIndex: "nguoiTao",
      key: "nguoiTao.hoTen",
      render: (item) => {
        return <span>{item && item.hoTen}</span>;
      },
    },
    {
      title: "Thao Tác",
      key: "action",
      render: (item) => {
        return (
          dataSource && (
            <>
              <div className="action">
                <button
                  onClick={() => {
                    handelRegister(item);
                  }}
                  className="bg-green-500 rounded py-2 text-white font-medium px-4 text-lg hover:bg-green-700 duration-300"
                >
                  Ghi Danh
                </button>
                <button
                  onClick={() => {
                    showModal(item);
                  }}
                  className="bg-orange-500 mx-2 rounded py-2 text-white font-medium px-4 text-lg hover:bg-orange-700 duration-300"
                >
                  Sửa
                </button>
                <button
                  onClick={() => {
                    handelDelCourses(item);
                  }}
                  className="bg-red-500 rounded py-2 text-white font-medium px-4 text-lg  hover:bg-red-700  duration-300"
                >
                  Xóa
                </button>
              </div>
            </>
          )
        );
      },
    },
  ];
  // Xử lý dropdown
  useEffect(() => {
    const handelClickOutSide = (event) => {
      if (toggleRef.current && !toggleRef.current.contains(event.target)) {
        setShow(false);
      }
    };
    document.addEventListener("click", handelClickOutSide);
    return () => {
      document.removeEventListener("click", handelClickOutSide);
    };
  }, []);
  // colum xác thực học viên tham gia khóa học
  const columsKhChoXacThuc = [
    {
      title: "Tài Khoản",
      dataIndex: "taiKhoan",
      key: "taiKhoan",
    },
    {
      title: "Học Viên",
      dataIndex: "hoTen",
      key: "hoTen",
    },
    {
      title: "Chờ xác nhận",
      key: "thaoTac",
      render: (item) => {
        return (
          <>
            <button
              onClick={() => {
                handelConfirmRegis(item);
              }}
              className="bg-green-500 rounded py-2 text-white font-medium px-4 text-lg hover:bg-green-700 duration-300 mx-2"
            >
              Xác thực
            </button>
            <button
              onClick={() => {
                handelDelConfirmRegis(item);
              }}
              className="bg-red-500 rounded py-2 text-white font-medium px-4 text-lg  hover:bg-red-700  duration-300"
            >
              Xóa
            </button>
          </>
        );
      },
    },
  ];
  // colum học viên đã tam gia khóa học
  const columsKhDaGhiDanh = [
    {
      title: "Tài Khoản",
      dataIndex: "taiKhoan",
      key: "taiKhoan",
    },
    {
      title: "Học Viên",
      dataIndex: "hoTen",
      key: "hoTen",
    },
    {
      title: "Chờ xác nhận",
      key: "thaoTac",
      render: (item) => {
        return (
          <>
            <button
              onClick={() => {
                handeleCancelRegis(item);
              }}
              className="bg-red-500 rounded py-2 text-white font-medium px-4 text-lg  hover:bg-red-700  duration-300"
            >
              Xóa
            </button>
          </>
        );
      },
    },
  ];
  const [formEdit] = Form.useForm();
  const showModal = (item) => {
    dispatch(setDynamicCourse(item));
    setIsModalOpen(true);
    const data = { ...item };
    formEdit.setFieldsValue(data);
  };
  const showModal2 = () => {
    setIsModalOpen2(true);
  };
  const showModal3 = () => {
    setIsModalOpen3(true);
  };
  const showModal4 = () => {
    setIsModalOpen4(true);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const handleCancel2 = () => {
    setIsModalOpen2(false);
  };
  const handleCancel3 = () => {
    setIsModalOpen3(false);
  };
  const handleCancel4 = () => {
    setIsModalOpen4(false);
  };
  // xử lý dropdown
  const handelToggle = () => {
    setShow(!show);
  };

  // config api
  const local = LocalService.getItem("USER_INFO");
  const config = {
    headers: {
      Authorization: `Bearer ${local.accessToken}`,
    },
  };

  const imgProps = {
    multiple: false,
    action: "https://localhost:3000/",
    accept: ".png,.jpg,.doc,.jpeg",
    listType: "picture",
    beforeUpload: (file) => {
      return false;
    },
  };

  //  xử lý sửa thông tin khóa học
  const onFinish = async (value) => {
    const data = {
      maKhoaHoc: value.maKhoaHoc,
      biDanh: dynamicCourse.biDanh,
      tenKhoaHoc: value.tenKhoaHoc,
      moTa: value.moTa,
      luotXem: value.luotXem,
      danhGia: value.danhGia,
      hinhAnh:
        typeof value.hinhAnh === "object"
          ? value.hinhAnh.file.name
          : `${value.hinhAnh}`,
      maNhom: value.maNhom,
      ngayTao: value.ngayTao,
      maDanhMucKhoaHoc: value.maDanhMuc,
      taiKhoanNguoiTao: value.nguoiTao.taiKhoan,
      nguoiTao: {
        hoTen: value.nguoiTao.hoTen,
        maLoaiNguoiDung: value.nguoiTao.maLoaiNguoiDung,
        taiKhoan: value.nguoiTao.taiKhoan,
        tenLoaiNguoiDung: value.nguoiTao.tenLoaiNguoiDung,
      },
    };
    try {
      const res = await https.put(
        "api/QuanLyKhoaHoc/CapNhatKhoaHoc",
        data,
        config
      );

      Swal.fire("Đã Sửa Thành Công!", "Cập nhật thành công!", "success");
      dispatch(updateCoursesManagement(data));
      setTimeout(() => {
        handleCancel();
      }, 1000);
      const formValue = new FormData();
      formValue.append("maKhoaHoc", res.data.maKhoaHoc);
      formValue.append("tenKhoaHoc", res.data.tenKhoaHoc);
      formValue.append("moTa", res.data.moTa);
      formValue.append("luotXem", res.data.luotXem);
      formValue.append("danhGia", res.data.danhGia);
      formValue.append("maNhom", res.data.maNhom);
      formValue.append("ngayTao", res.data.ngayTao);
      formValue.append("maDanhMucKhoaHoc", res.data.maDanhMucKhoaHoc);
      formValue.append("taiKhoanNguoiTao", res.data.taiKhoanNguoiTao);
      formValue.append("hinhAnh", value.hinhAnh.file);

      const uploadResponse = await https.post(
        "api/QuanLyKhoaHoc/CapNhatKhoaHocUpload",
        formValue
      );
    } catch (error) {
     
    }
  };

  // xử lý tạo khóa học
  const handelAddCourse = (value) => {

    let data = {
      ...value,
      hinhAnh: value.hinhAnh.file.name,
      taiKhoanNguoiTao: user.taiKhoan,
    };

    https
      .post("api/QuanLyKhoaHoc/ThemKhoaHoc", data, config)
      .then((res) => {
    
        dispatch(createCourse(res.data));
        Swal.fire("Đã Thêm Thành Công!", "Thêm khóa học thành công!", "success");
        setTimeout(() => {
          handleCancel2();
        }, 1000);
      })
      .catch((err) => {
     
        Swal.fire("Thêm khóa học Thất bại!", err.response.data, "warning");
      });
  };
  //  xử lý xóa khóa học
  const handelDelCourses = (item) => {
    const data = item.maKhoaHoc;
    https
      .delete(`api/QuanLyKhoaHoc/XoaKhoaHoc?MaKhoaHoc=${data}`, config)
      .then((res) => {
        dispatch(delCoursesManagement(item));
        Swal.fire("Đã Xóa!", "Cập nhật thành công!", "success");
      })
      .catch((err) => {
        Swal.fire("Xóa Thất bại!", err.response.data, "warning");
      });
  };

  // xử lý tìm kiếm khóa học
  const handelOnSearch = (e) => {
    const value = e.target.value;
    https
      .get(`api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?tenKhoaHoc=${value}`)
      .then((res) => {
        if (res.data.length === 0) {
          dispatch(searchCoursesManagement(dataSearch));
        } else {
          dispatch(searchCoursesManagement(res.data));
        }
      })
      .catch((err) => {});
  };

  //  xử mở modal ghi danh
  const handelRegister = (item) => {
    dispatch(setDynamic(item));

    showModal3();
    const data = {
      maKhoaHoc: item.maKhoaHoc, 

    };
    // lấy danh sách học viên đã tham gia  khóa học
    https
      .post("api/QuanLyNguoiDung/LayDanhSachHocVienKhoaHoc", data, config)
      .then((res) => {
        dispatch(setListStudentAttended(res.data));
      })
      .catch((err) => {});
    //  lấy danh sách học viên chưa tham gia khoá học
    https
      .post("api/QuanLyNguoiDung/LayDanhSachNguoiDungChuaGhiDanh", data, config)
      .then((res) => {
        dispatch(setListStudentInAttend(res.data));
      })
      .catch((err) => {});
    // lấy danh sách học viên  chờ xét duyệt
    https
      .post("api/QuanLyNguoiDung/LayDanhSachHocVienChoXetDuyet", data, config)
      .then((res) => {
        dispatch(setListStudentPendingAttend(res.data));
      })
      .catch((err) => {});
  };
  //  xử lý xóa học viên tham gia khóa học
  const handeleCancelRegis = (item) => {
    const data = {
      maKhoaHoc: dynamic.maKhoaHoc,
      taiKhoan: item.taiKhoan,
    };
    https
      .post("api/QuanLyKhoaHoc/HuyGhiDanh", data, config)
      .then((res) => {
        Swal.fire("Đã Xóa", "Xóa thành công!", "success");
        dispatch(delStudentAttended(item));
      })
      .catch((err) => {});
  };
  //  xử lý xác thực ghi danh
  const handelConfirmRegis = (item) => {
    const data = {
      maKhoaHoc: dynamic.maKhoaHoc,
      taiKhoan: item.taiKhoan,
    };
    https
      .post("api/QuanLyKhoaHoc/GhiDanhKhoaHoc", data, config)
      .then((res) => {
        dispatch(confirmStudentAttend(item));
        Swal.fire("Đã Ghi Danh Thành Công", "Thành công!", "success");
      })
      .catch((err) => {});
  };
  //  xử lý xóa học viên chờ xác thực ghi danh
  const handelDelConfirmRegis = (item) => {
    const data = {
      maKhoaHoc: dynamic.maKhoaHoc,
      taiKhoan: item.taiKhoan,
    };
    https
      .post("api/QuanLyKhoaHoc/HuyGhiDanh", data, config)
      .then((res) => {
        dispatch(delStudentPendingAttend(item));
        Swal.fire("Đã Hủy", "Hủy thành công!", "success");
      })
      .catch((err) => {
      });
  };
  // xử lý update thông tin giáo vụ
  const updateInfo = (value) => {
    const data = {
      ...value,
      accessToken: user.accessToken,
      maNhom: "GP01",
    };
    https
      .put("api/QuanLyNguoiDung/CapNhatThongTinNguoiDung", data, config)
      .then((res) => {
        dispatch(update(data));
        Swal.fire("Cập Nhật Thành Công", " Thành công!", "success");
        setTimeout(() => {
          handleCancel4();
        }, 1000);
      })
      .catch((err) => {});
  };
  //  xử lý admin đăng ký khóa học
  const handelAdRegis = (value) => {
    const data = {
      ...value,
      maKhoaHoc: dynamic.maKhoaHoc,
    };    
    https
      .post("api/QuanLyKhoaHoc/GhiDanhKhoaHoc", data, config)
      .then((res) => {
        Swal.fire("Ghi Danh Thành Công", " Thành công!", "success");
      })
      .catch((err) => {});
  };

  return (
    <>
      {/* modal sửa thông tin khóa học */}
      <Modal
        className="modal"
        open={isModalOpen}
        width={800}
        onCancel={handleCancel}
      >
        <p className="text-2xl text-white text-center py-2 uppercase">
          Cập nhật
        </p>
        <Form
          action="api/QuanLyKhoaHoc/CapNhatKhoaHocUpload"
          method="POST"
          className="grid grid-cols-2 gap-3"
          name="formEditCourse"
          form={formEdit}
          onFinish={onFinish}
        >
          <Form.Item name="maKhoaHoc">
            <Input
              disabled
              placeholder="Mã khóa học"
              prefix={<SmileOutlined className="mr-2" />}
              size="large"
              className="text-lg"
            />
          </Form.Item>
          <Form.Item
            name="tenKhoaHoc"
            rules={[
              {
                required: "true",
                message: "Tên khóa học không được bỏ trống",
              },
            ]}
          >
            <Input
              prefix={<BookOutlined className="mr-2" />}
              placeholder="Tên khóa học"
              size="large"
              className="text-lg"
            />
          </Form.Item>

          <Form.Item
            name="maDanhMuc"
            rules={[
              {
                required: "true",
                message: "Danh mục khóa học không được bỏ trống",
              },
            ]}
          >
            <Select
              placeholder="Chọn danh mục khóa học"
              size="large"
              className="text-lg text-black font-bold"
            >
              {catalogCourses.map((item, index) => {
                return (
                  <Select.Option value={item.maDanhMuc} key={index}>
                    <span className="text-lg">{item.tenDanhMuc}</span>
                  </Select.Option>
                );
              })}
            </Select>
          </Form.Item>
          <Form.Item
            className="text-white"
            name="ngayTao"
            rules={[
              {
                required: "true",
                message: "Ngày tạo không được bỏ trống",
              },
            ]}
          >
            <Input
              className="text-lg"
              prefix={<CalendarOutlined className="mr-2" />}
              placeholder="Ngày tạo"
              size="large"
            />
          </Form.Item>
          <Form.Item
            name="danhGia"
            rules={[
              {
                required: "true",
                message: "Đánh giá không được bỏ trống",
              },
            ]}
          >
            <InputNumber
              placeholder="Đánh giá"
              className="w-full h-full text-lg"
              min={1}
              max={5}
            />
          </Form.Item>
          <Form.Item
            name="luotXem"
            rules={[
              {
                required: "true",
                message: "Lượt xem không được bỏ trống",
              },
            ]}
          >
            <InputNumber
              placeholder="Lượt xem"
              className="w-full h-full text-lg"
              min={1}
            />
          </Form.Item>
          <Form.Item
            className="row-span-2 w-full text-white"
            name="hinhAnh"
            rules={[
              {
                required: "true",
                message: "Hình ảnh không được bỏ trống",
              },
            ]}
          >
            <Upload {...imgProps} className="w-full py-2">
              <Button
                size="large"
                className="w-full text-white text-xl "
                icon={<UploadOutlined />}
              >
                Chọn hình ảnh để thêm vào{" "}
              </Button>
            </Upload>
          </Form.Item>
          <Form.Item
            className="row-span-2"
            name="maNhom"
            rules={[
              {
                required: "true",
                message: "Người tạo không được bỏ trống",
              },
            ]}
          >
            <Select placeholder="Chọn mã nhóm" size="large" className="text-lg">
              <Select.Option value="GP01">GP01</Select.Option>
              <Select.Option value="GP02">GP02</Select.Option>
              <Select.Option value="GP03">GP03</Select.Option>
              <Select.Option value="GP04">GP04</Select.Option>
              <Select.Option value="GP05">GP05</Select.Option>
              <Select.Option value="GP06">GP06</Select.Option>
              <Select.Option value="GP07">GP07</Select.Option>
              <Select.Option value="GP08">GP08</Select.Option>
              <Select.Option value="GP09">GP09</Select.Option>
              <Select.Option value="GP10">GP10</Select.Option>
              <Select.Option value="GP11">GP11</Select.Option>
              <Select.Option value="GP12">GP12</Select.Option>
              <Select.Option value="GP13">GP13</Select.Option>
              <Select.Option value="GP14">GP14</Select.Option>
              <Select.Option value="GP15">GP15</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item name="nguoiTao"></Form.Item>
          <h6 className="text-center text-xl py-2 text-white font-bold border-b-2 border-white w-full col-span-2">
            Mô tả khóa học
          </h6>
          <Form.Item
            wrapperCol={{
              offset: 5,
              span: 4,
            }}
          >
            <Image src={dynamicCourse.hinhAnh} width={200} height={200} />
          </Form.Item>
          <Form.Item name="moTa">
            <Input.TextArea
              className="text-lg"
              autoSize={{ minRows: 6, maxRows: 10 }}
              placeholder="Mô tả khóa học"
            />
          </Form.Item>
          <Form.Item
            wrapperCol={{
              offset: 24,
              span: 8,
            }}
          >
            <Button
              form="formEditCourse"
              htmlType="submit"
              size="large"
              className="text-white bg-green-400"
            >
              {" "}
              Cập nhật
            </Button>
          </Form.Item>
        </Form>
      </Modal>
      {/* modal tạo khóa học */}
      <Modal
        className="modal"
        open={isModalOpen2}
        width={800}
        onCancel={handleCancel2}
      >
        <p className="text-2xl text-white text-center py-2 uppercase">
          thêm khóa học
        </p>
        <Form className="grid grid-cols-2 gap-3" onFinish={handelAddCourse}>
          <Form.Item name="maKhoaHoc">
            <Input
              placeholder="Mã khóa học"
              prefix={<SmileOutlined className="mr-2" />}
              size="large"
              className="text-lg"
            />
          </Form.Item>
          <Form.Item
            name="tenKhoaHoc"
            rules={[
              {
                required: "true",
                message: "Tên khóa học không được bỏ trống",
              },
            ]}
          >
            <Input
              prefix={<BookOutlined className="mr-2" />}
              placeholder="Tên khóa học"
              size="large"
              className="text-lg"
            />
          </Form.Item>
          <Form.Item
            name="biDanh"
            rules={[
              {
                required: "true",
                message: "Bí danh học không được bỏ trống",
              },
            ]}
          >
            <Input
              prefix={<BookOutlined className="mr-2" />}
              placeholder="Bí danh khóa học"
              size="large"
              className="text-lg"
            />
          </Form.Item>
          <Form.Item
            name="maDanhMucKhoaHoc"
            rules={[
              {
                required: "true",
                message: "Danh mục khóa học không được bỏ trống",
              },
            ]}
          >
            <Select
              placeholder="Chọn danh mục khóa học"
              size="large"
              className="text-lg text-black font-bold"
            >
              {catalogCourses.map((item, index) => {
                return (
                  <Select.Option value={item.maDanhMuc} key={index}>
                    <span className="text-lg">{item.tenDanhMuc}</span>
                  </Select.Option>
                );
              })}
            </Select>
          </Form.Item>
          <Form.Item
            className="text-white"
            name="ngayTao"
            rules={[
              {
                required: "true",
                message: "Ngày tạo không được bỏ trống",
              },
              {
                pattern: regex.date,
                message: "Vui lòng nhập đúng định dạng DD/MM/YYYY",
              },
            ]}
          >
            <Input
              className="text-lg"
              prefix={<CalendarOutlined className="mr-2" />}
              placeholder="Ngày tạo"
              size="large"
            />
          </Form.Item>
          <Form.Item
            name="danhGia"
            rules={[
              {
                required: "true",
                message: "Đánh giá không được bỏ trống",
              },
            ]}
          >
            <InputNumber
              placeholder="Đánh giá"
              className="w-full h-full text-lg"
              min={1}
              max={5}
            />
          </Form.Item>
          <Form.Item
            name="luotXem"
            rules={[
              {
                required: "true",
                message: "Lượt xem không được bỏ trống",
              },
            ]}
          >
            <InputNumber
              placeholder="Lượt xem"
              className="w-full h-full text-lg"
              min={1}
            />
          </Form.Item>
          <Form.Item
            className="row-span-2 w-full text-white"
            name="hinhAnh"
            rules={[
              {
                required: "true",
                message: "Hình ảnh không được bỏ trống",
              },
            ]}
          >
            <Upload {...imgProps} className="w-full py-2">
              <Button
                size="large"
                className="w-full text-white text-xl "
                icon={<UploadOutlined />}
              >
                Chọn hình ảnh để thêm vào{" "}
              </Button>
            </Upload>
          </Form.Item>
          <Form.Item
            className="row-span-2"
            name="maNhom"
            rules={[
              {
                required: "true",
                message: "Người tạo không được bỏ trống",
              },
            ]}
          >
            <Select placeholder="Chọn mã nhóm" size="large" className="text-lg">
              <Select.Option value="GP01">GP01</Select.Option>
              <Select.Option value="GP02">GP02</Select.Option>
              <Select.Option value="GP03">GP03</Select.Option>
              <Select.Option value="GP04">GP04</Select.Option>
              <Select.Option value="GP05">GP05</Select.Option>
              <Select.Option value="GP06">GP06</Select.Option>
              <Select.Option value="GP07">GP07</Select.Option>
              <Select.Option value="GP08">GP08</Select.Option>
              <Select.Option value="GP09">GP09</Select.Option>
              <Select.Option value="GP10">GP10</Select.Option>
              <Select.Option value="GP11">GP11</Select.Option>
              <Select.Option value="GP12">GP12</Select.Option>
              <Select.Option value="GP13">GP13</Select.Option>
              <Select.Option value="GP14">GP14</Select.Option>
              <Select.Option value="GP15">GP15</Select.Option>
            </Select>
          </Form.Item>
          <h6 className="text-center text-xl py-2 text-white font-bold border-b-2 border-white w-full col-span-2">
            Mô tả khóa học
          </h6>
          <Form.Item
            wrapperCol={{
              offset: 5,
              span: 4,
            }}
          >
            <Image
              src="https://statics.cdn.200lab.io/2021/07/1_h5UGPzaL1E4dIy_JWDrsAw.png"
              width={200}
              height={200}
            />
          </Form.Item>
          <Form.Item name="moTa">
            <Input.TextArea
              className="text-lg"
              autoSize={{ minRows: 6, maxRows: 10 }}
              placeholder="Mô tả khóa học"
            />
          </Form.Item>
          <Form.Item
            wrapperCol={{
              offset: 20,
              span: 12,
            }}
          >
            <Button
              htmlType="submit"
              size="large"
              className="text-white bg-green-400"
            >
              {" "}
              Thêm Khóa Học
            </Button>
          </Form.Item>
        </Form>
      </Modal>
      {/* modal ghi danh */}
      <Modal
        width={800}
        className="modal text-xl"
        open={isModalOpen3}
        onCancel={handleCancel3}
      >
        <div className=" grid px-2 bg-white grid-cols-6 border-none text-center rounded-lg mb-3 uppercase border-b-2 ">
          <h6 className="text-lg pt-3 col-span-2 font-medium">
            chọn người dùng
          </h6>
          <Form
            onFinish={handelAdRegis}
            className=" col-span-4 flex  items-center justify-between"
          >
            <Form.Item
              className="w-3/4 pt-3"
              name="taiKhoan"
              wrapperCol={{
                offset: 1,
                span: 24,
              }}
            >
              <Select
                className="font-bold text-3xl"
                placeholder="Chọn học viên"
              >
                {listStudentInAttend.map((item, index) => {
                  return (
                    <Select.Option value={item.taiKhoan} key={index}>
                      <span className="text-lg">{item.hoTen}</span>
                    </Select.Option>
                  );
                })}
              </Select>
            </Form.Item>
            <Form.Item
              className="pt-3"
              wrapperCol={{
                offset: 1,
                span: 24,
              }}
            >
              <Button
                htmlType="submit"
                className="bg-green-500 mr-2  flex items-center justify-center rounded px-2 py-1 text-white font-medium  text-lg hover:bg-green-700 duration-300"
              >
                <span className="">Ghi danh</span>
              </Button>
            </Form.Item>
          </Form>
        </div>
        <div className="py-2">
          <h6 className="text-xl font-bold text-white">
            Học viên chờ xác thực
          </h6>
          <Table
            dataSource={listStudentPendingAttend}
            columns={columsKhChoXacThuc}
            pagination={{
              pageSize: 2,
              position: ["bottomCenter"],
            }}
          />
        </div>
        <div className="py-2">
          <h6 className="text-xl font-bold text-white">Học viên đã ghi danh</h6>
          <Table
            columns={columsKhDaGhiDanh}
            dataSource={listStudentAttended}
            pagination={{
              pageSize: 2,
              position: ["bottomCenter"],
            }}
          />
        </div>
      </Modal>
      {/* modal cập nhập thông tin giáo vụ  */}
      <Modal className="modal" open={isModalOpen4} onCancel={handleCancel4}>
        <p className="text-2xl text-white text-center py-2 uppercase">
          thông tin cá nhân
        </p>
        <Form autoComplete="off" name="form_add_cource" onFinish={updateInfo}>
          <Form.Item initialValue={user.taiKhoan} name="taiKhoan">
            <Input
              disabled
              prefix={<SmileOutlined className="mr-2" />}
              size="large"
            />
          </Form.Item>
          <Form.Item
            name="matKhau"
            rules={[
              {
                required: "true",
                message: "Mật khẩu không được bỏ trống",
              },
              {
                pattern: regex.password,
                message:
                  "Mật khẩu gồm 8-15 ký tự, 1 ký tự viết hoa, 1 số, 1 ký tự đặc biệt",
              },
            ]}
          >
            <Input.Password
              prefix={<LockOutlined className="mr-2" />}
              placeholder="Mật Khẩu"
              size="large"
            />
          </Form.Item>
          <Form.Item
            initialValue={user.hoTen}
            name="hoTen"
            rules={[
              {
                required: "true",
                message: "Họ tên không được bỏ trống",
              },
            ]}
          >
            <Input
              prefix={<UserOutlined className="mr-2" />}
              placeholder="Họ và tên"
              size="large"
            />
          </Form.Item>
          <Form.Item
            initialValue={user.email}
            name="email"
            rules={[
              {
                required: "true",
                message: "Email không được bỏ trống",
              },
              {
                type: "email",
                message: "Nhập đúng định dạng email",
              },
            ]}
          >
            <Input
              prefix={<MailOutlined className="mr-2" />}
              placeholder="Email"
              size="large"
            />
          </Form.Item>
          <Form.Item
            initialValue={user.soDT}
            name="soDt"
            rules={[
              {
                required: "true",
                message: "Email không được bỏ trống",
              },
              {
                pattern: regex.phone,
                message: "Nhập đúng định dạng số điện thoại",
              },
            ]}
          >
            <Input
              prefix={<PhoneOutlined className="mr-2" />}
              placeholder="Số điện thoại"
              size="large"
            />
          </Form.Item>
          <Form.Item
            name="maLoaiNguoiDung"
            rules={[
              {
                required: "true",
                message: "Vui lòng chọn loại người dùng",
              },
            ]}
          >
            <Select placeholder="Loại người dùng" size="large">
              <Select.Option value="GV">Giáo Vụ</Select.Option>
              <Select.Option value="HV">Học Viên</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item
            wrapperCol={{
              offset: 10,
              span: 5,
            }}
          >
            <Button
              form="form_add_cource"
              htmlType="submit"
              size="large"
              className="text-white bg-green-400"
            >
              {" "}
              Cập nhật
            </Button>
          </Form.Item>
        </Form>
      </Modal>
      {/*  */}
      <div className="containerbg ">
        <div className="dashboard p-5">
          <div className="navbar">
            <div className="navbarHeader pt-4">
              <a
                href="/"
                className="bg-white px-2 py-4 text-black border-white btn"
              >
                <span>
                  <i className="fa fa-home"></i>
                </span>
              </a>
            </div>
            <div className="navtitle">
              <ul className="py-20">
                <li className="my-5">
                  <NavLink to="/admin/quanlynguoidung">
                    <i className="fa fa-user"></i>
                    Quản lý người dùng
                  </NavLink>
                </li>
                <li>
                  <NavLink to="/admin/quanlykhoahoc">
                    <i className="fa fa-briefcase"></i>
                    Quản lý khóa học
                  </NavLink>
                </li>
              </ul>
            </div>
          </div>
          <div className="detail card p-5">
            <div className="cardHeader px-5">
              <div className="grid grid-cols-2">
                <div className="button">
                  <button
                    onClick={showModal2}
                    className="bg-green-500 rounded py-2 text-white font-medium px-4 text-lg hover:bg-green-700 duration-300"
                  >
                    Thêm Khóa Học
                  </button>
                </div>
                <div className="search flex justify-between">
                  <form action="">
                    <input
                      onChange={handelOnSearch}
                      className="form-control"
                      type="text"
                      placeholder="Nhập tên khóa học"
                    />
                  </form>
                  <div className="img flex space-x-3 items-center">
                    <span className="font-bold">Chào {user.hoTen}</span>
                    <div ref={toggleRef} className="icon">
                      <button onClick={handelToggle}>
                        <img
                          className="rounded-full"
                          style={{ width: "40px", height: "40px" }}
                          src="https://cdn.dribbble.com/users/2364329/screenshots/6676961/02.jpg?compress=1&resize=800x600"
                          alt="img"
                        />
                      </button>
                      <div
                        className={
                          show
                            ? `on dropdown-content-admin  `
                            : "dropdown-content-admin"
                        }
                      >
                        <ul>
                          <li className="py-2">
                            <button onClick={showModal4}>
                              <i className="fa fa-user-circle mr-2"></i>
                              Cập nhật thông tin
                            </button>
                          </li>
                          <li className="py-2">
                            <button
                              onClick={() => {
                                LocalService.removeItem("USER_INFO");
                                window.location.reload();
                              }}
                            >
                              <i className="mr-2 fa fa-sign-out-alt"></i>
                              Đăng xuất
                            </button>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="table h-screen p-5">
              <Table
                size="large
            "
                pagination={{
                  pageSize: 6,
                  position: ["bottomCenter"],
                }}
                dataSource={dataSource && dataSource}
                columns={dataSource && columns}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
