import React from "react";


import { Desktop, Mobile, Tablet } from "../../responsive/responsive";
import CoursesManagementDesktop from "./CousesManagementDesktop";
import CoursesManagementTablet from "./CousesManagementTablet";
import CoursesManagementMobile from "./CousesManagementMobile";

export default function CoursesManagement() {


  return (
    <>
    
      <Desktop>
        <CoursesManagementDesktop/>
      </Desktop>

      <Tablet>
        <CoursesManagementTablet/>
      </Tablet>

      <Mobile>
      <CoursesManagementMobile/>
      </Mobile>
    </>
  );
}
