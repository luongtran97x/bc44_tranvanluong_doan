import React from "react";

import UserManagementDesktop from "./UserManagementDesktop";
import { Desktop, Mobile, Tablet } from "../../responsive/responsive";
import UserManagementTablet from "./UserManagementTablet";
import UserManagementMobile from "./UserManagementMobile";

export default function UserManagement() {
  return (
    <>
      <Desktop>
        <UserManagementDesktop />
      </Desktop>

      <Tablet>
        <UserManagementTablet />
      </Tablet>

      <Mobile>
        <UserManagementMobile />
      </Mobile>
    </>
  );
}
