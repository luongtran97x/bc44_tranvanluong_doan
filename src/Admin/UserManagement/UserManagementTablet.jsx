import React, { useRef, useState } from "react";
import "../Admin.css";
import { Button, Form, Input, Modal, Select, Table, message } from "antd";
import { useEffect } from "react";
import { https } from "../../services/config";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import {
  addUserManagement,
  cancelRegis,
  confirmRegis,
  delConfirmRegis,
  delUserManagement,
  searchUserManagement,
  setListPendingRegistedCourses,
  setListRegistedCourses,
  setListRegisterCourses,
  setUserDynamic,
  setUserManagement,
  updateUserManagement,
} from "../../redux/Admin/userManagementSlice";
import { regex } from "../../Constant/regex";
import { LocalService } from "../../services/LocalService";
import {
  LockOutlined,
  MailOutlined,
  UserOutlined,
  PhoneOutlined,
  SmileOutlined,
  MenuOutlined,
  RadarChartOutlined,
  HomeOutlined,
} from "@ant-design/icons";
import Swal from "sweetalert2";
import { update } from "../../redux/userSlice";

export default function UserManagementTablet() {
  const toggleRef = useRef(null);
  const dispatch = useDispatch();
  const [dataSearch, setDataSearch] = useState([]);
  const dataSource = useSelector(
    (state) => state.userManagement.userManagement
  );
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isModalOpen2, setIsModalOpen2] = useState(false);
  const [isModalOpen3, setIsModalOpen3] = useState(false);
  const [isModalOpen4, setIsModalOpen4] = useState(false);
  const [show, setShow] = useState(false);
  const user = useSelector((state) => state.userSlice.userInfo);
  const listRegisterCourses = useSelector(
    (state) => state.userManagement.listRegisterCourses
  );
  const listRegistedCourses = useSelector(
    (state) => state.userManagement.listRegistedCourses
  );
  const listPendingRegistedCourses = useSelector(
    (state) => state.userManagement.listPendingRegistedCourses
  );
  const userDynamic = useSelector((state) => state.userManagement.userDynamic);
  // Lấy danh sách người dùng
  useEffect(() => {
    https
      .get("api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP01")
      .then((res) => {
        dispatch(setUserManagement(res.data));
        setDataSearch(res.data);
      })
      .catch((err) => {});
  }, []);
  const columns = [
    {
      title: "Tài khoản",
      dataIndex: "taiKhoan",
      key: "taiKhoan",
    },
    {
      title: "Người dùng",
      dataIndex: "maLoaiNguoiDung",
      key: "maLoaiNguoiDung",
      render: (maLoaiNguoiDung) => {
        return (
          dataSource && (
            <span key={maLoaiNguoiDung}>
              {maLoaiNguoiDung === "GV" ? "Giáo Vụ" : "Học Viên"}
            </span>
          )
        );
      },
    },
    {
      title: "Họ và tên",
      dataIndex: "hoTen",
      key: "hoTen",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Số điện thoại",
      dataIndex: "soDt",
      key: "soDt",
    },
    {
      title: "Thao Tác",
      key: "action",
      render: (item) => {
        return (
          dataSource && (
            <>
              <div className="action items-center justify-center flex flex-col space-y-2">
                <button
                  onClick={() => {
                    handelRegister(item);
                  }}
                  className="bg-green-500 rounded py-2 text-white font-medium px-4 text-lg hover:bg-green-700 duration-300"
                >
                  Ghi Danh
                </button>
                <button
                  onClick={() => {
                    showModal(item);
                  }}
                  className="bg-orange-500 mx-2 rounded py-2 text-white font-medium px-4 text-lg hover:bg-orange-700 duration-300"
                >
                  Sửa
                </button>
                <button
                  onClick={() => {
                    handelDelUser(item);
                  }}
                  className="bg-red-500 rounded py-2 text-white font-medium px-4 text-lg  hover:bg-red-700  duration-300"
                >
                  Xóa
                </button>
              </div>
            </>
          )
        );
      },
    },
  ];
  // Xử lý dropdown
  useEffect(() => {
    const handelClickOutSide = (event) => {
      if (toggleRef.current && !toggleRef.current.contains(event.target)) {
        setShow(false);
      }
    };
    document.addEventListener("click", handelClickOutSide);
    return () => {
      document.removeEventListener("click", handelClickOutSide);
    };
  }, []);
  const columsKhChoXacThuc = [
    {
      title: "Tên khóa học",
      dataIndex: "tenKhoaHoc",
      key: "tenKhoaHoc",
    },
    {
      title: "Chờ xác nhận",
      key: "thaoTac",
      render: (item) => {
        return (
          <>
            <button
              onClick={() => {
                handelConfirmRegis(item);
              }}
              className="bg-green-500 rounded py-2 text-white font-medium px-4 text-lg hover:bg-green-700 duration-300 mx-2"
            >
              Xác thực
            </button>
            <button
              onClick={() => {
                handelDelConfirmRegis(item);
              }}
              className="bg-red-500 rounded py-2 text-white font-medium px-4 text-lg  hover:bg-red-700  duration-300"
            >
              Xóa
            </button>
          </>
        );
      },
    },
  ];

  const columsKhDaGhiDanh = [
    {
      title: "Tên khóa học",
      dataIndex: "tenKhoaHoc",
      key: "tenKhoaHoc",
    },
    {
      title: "Chờ xác nhận",
      key: "thaoTac",
      render: (item) => {
        return (
          <>
            <button
              onClick={() => {
                handeleCancelRegis(item);
              }}
              className="bg-red-500 rounded py-2 text-white font-medium px-4 text-lg  hover:bg-red-700  duration-300"
            >
              Xóa
            </button>
          </>
        );
      },
    },
  ];
  const [formEdit] = Form.useForm();
  const showModal = (item) => {
    setIsModalOpen(true);
    const data = { ...item };
    formEdit.setFieldsValue(data);
  };
  const showModal2 = () => {
    setIsModalOpen2(true);
  };
  const showModal3 = () => {
    setIsModalOpen3(true);
  };
  const showModal4 = () => {
    setIsModalOpen4(true);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const handleCancel2 = () => {
    setIsModalOpen2(false);
  };
  const handleCancel3 = () => {
    setIsModalOpen3(false);
  };
  const handleCancel4 = () => {
    setIsModalOpen4(false);
  };
  // xử lý dropdown
  const handelToggle = () => {
    setShow(!show);
  };

  // config api
  const local = LocalService.getItem("USER_INFO");
  const config = {
    headers: {
      Authorization: `Bearer ${local.accessToken}`,
    },
  };

  //  xử lý sửa thông tin người dùng
  const onFinish = (value) => {
    const data = {
      ...value,
      maNhom: "GP01",
    };
    https
      .put("api/QuanLyNguoiDung/CapNhatThongTinNguoiDung", data, config)
      .then((res) => {
        Swal.fire("Đã Sửa Thành Công!", "Cập nhật thành công!", "success");
        dispatch(updateUserManagement(data));
        setTimeout(() => {
          handleCancel();
        }, 1000);
      })
      .catch((err) => {
        Swal.fire("Sửa Thông Tin  Thất bại!", err.response.data, "warning");
      });
  };

  // xử lý thêm người dùng
  const handelAddUser = (value) => {
    let data = {
      ...value,
      maNhom: "GP01",
    };

    https
      .post("api/QuanLyNguoiDung/ThemNguoiDung", data, config)
      .then((res) => {
        dispatch(addUserManagement(res.data));
        Swal.fire("Đã Thêm Thành Công!", "Cập nhật thành công!", "success");
        setTimeout(() => {
          handleCancel2();
        }, 1000);
      })
      .catch((err) => {
        console.log("🚀 ~ err:", err);
        Swal.fire("Thêm Người DùngThất bại!", err.response.data, "warning");
      });
  };
  //  xử lý xóa người dùng
  const handelDelUser = (item) => {
    const data = item.taiKhoan;
    https
      .delete(`api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${data}`, config)
      .then((res) => {
        dispatch(delUserManagement(item));
        Swal.fire("Đã Xóa!", "Cập nhật thành công!", "success");
      })
      .catch((err) => {
        Swal.fire("Xóa Thất bại!", err.response.data, "warning");
      });
  };

  // xử lý tìm kiếm
  const handelOnSearch = (e) => {
    const value = e.target.value;
    https
      .get(`api/QuanLyNguoiDung/TimKiemNguoiDung?MaNhom=GP01&tuKhoa=${value}`)
      .then((res) => {
        if (res.data.length === 0) {
          dispatch(searchUserManagement(dataSearch));
        } else {
          dispatch(searchUserManagement(res.data));
        }
      })
      .catch((err) => {});
  };

  //  xử mở modal ghi danh
  const handelRegister = (item) => {
    dispatch(setUserDynamic(item));
    showModal3();
    const data = {
      taiKhoan: item.taiKhoan,
    };
    // lấy danh sách khóa học
    https
      .post(
        `api/QuanLyNguoiDung/LayDanhSachKhoaHocChuaGhiDanh?TaiKhoan=${item.taiKhoan}`,
        null,
        config
      )
      .then((res) => {
        dispatch(setListRegisterCourses(res.data));
      })
      .catch((err) => {});
    //  lấy danh sách khóa học đã xét duyệt
    https
      .post("api/QuanLyNguoiDung/LayDanhSachKhoaHocDaXetDuyet", data, config)
      .then((res) => {
        dispatch(setListRegistedCourses(res.data));
      })
      .catch((err) => {});
    // lấy danh sách khóa học chờ xét duyệt
    https
      .post("api/QuanLyNguoiDung/LayDanhSachKhoaHocChoXetDuyet", data, config)
      .then((res) => {
        dispatch(setListPendingRegistedCourses(res.data));
      })
      .catch((err) => {});
  };
  //  xử lý hủy ghi danh
  const handeleCancelRegis = (item) => {
    const data = {
      maKhoaHoc: item.maKhoaHoc,
      taiKhoan: userDynamic.taiKhoan,
    };
    https
      .post("api/QuanLyKhoaHoc/HuyGhiDanh", data, config)
      .then((res) => {
        Swal.fire("Đã Hủy", "Hủy thành công!", "success");
        dispatch(cancelRegis(item));
      })
      .catch((err) => {
        console.log("🚀 ~ err:", err);
      });
  };
  //  xử lý xác thực ghi danh
  const handelConfirmRegis = (item) => {
    const data = {
      maKhoaHoc: item.maKhoaHoc,
      taiKhoan: userDynamic.taiKhoan,
    };
    https
      .post("api/QuanLyKhoaHoc/GhiDanhKhoaHoc", data, config)
      .then((res) => {
        dispatch(confirmRegis(item));
        Swal.fire("Đã Ghi Danh Thành Công", "Thành công!", "success");
      })
      .catch((err) => {
        console.log("🚀 ~ err:", err);
      });

    console.log("🚀 ~ item:", item);
  };
  //  xử lý xóa xác thực ghi danh
  const handelDelConfirmRegis = (item) => {
    const data = {
      maKhoaHoc: item.maKhoaHoc,
      taiKhoan: userDynamic.taiKhoan,
    };
    https
      .post("api/QuanLyKhoaHoc/HuyGhiDanh", data, config)
      .then((res) => {
        dispatch(delConfirmRegis(item));
        Swal.fire("Đã Hủy", "Hủy thành công!", "success");
      })
      .catch((err) => {
        console.log("🚀 ~ err:", err);
      });
  };
  // xử lý update thông tin giáo vụ
  const updateInfo = (value) => {
    const data = {
      ...value,
      accessToken: user.accessToken,
      maNhom: "GP01",
    };
    https
      .put("api/QuanLyNguoiDung/CapNhatThongTinNguoiDung", data, config)
      .then((res) => {
        dispatch(update(data));
        Swal.fire("Cập Nhật Thành Công", " Thành công!", "success");
        setTimeout(() => {
          handleCancel4();
        }, 1000);
      })
      .catch((err) => {});
  };
  //  xử lý admin đăng ký khóa học
  const handelAdRegis = (value) => {
    const data = {
      ...value,
      taiKhoan: userDynamic.taiKhoan,
    };
    https
      .post("api/QuanLyKhoaHoc/GhiDanhKhoaHoc", data, config)
      .then((res) => {
        Swal.fire("Cập Nhật Thành Công", " Thành công!", "success");
      })
      .catch((err) => {});
  };

  return (
    <>
      {/* modal Cập nhập thông tin người dùng */}
      <Modal
        width={600}
        className="modal"
        open={isModalOpen}
        onCancel={handleCancel}
      >
        <p className="text-2xl text-white text-center py-2 uppercase">
          thông tin cá nhân
        </p>
        <Form
          autoComplete="off"
          name="form_add_cource"
          form={formEdit}
          onFinish={onFinish}
        >
          <Form.Item name="taiKhoan">
            <Input
              disabled
              prefix={<SmileOutlined className="mr-2" />}
              size="large"
            />
          </Form.Item>
          <Form.Item
            name="matKhau"
            rules={[
              {
                required: "true",
                message: "Mật khẩu không được bỏ trống",
              },
              {
                pattern: regex.password,
                message:
                  "Mật khẩu gồm 8-15 ký tự, 1 ký tự viết hoa, 1 số, 1 ký tự đặc biệt",
              },
            ]}
          >
            <Input.Password
              prefix={<LockOutlined className="mr-2" />}
              placeholder="Mật Khẩu"
              size="large"
            />
          </Form.Item>
          <Form.Item
            name="hoTen"
            rules={[
              {
                required: "true",
                message: "Họ tên không được bỏ trống",
              },
            ]}
          >
            <Input
              prefix={<UserOutlined className="mr-2" />}
              placeholder="Họ và tên"
              size="large"
            />
          </Form.Item>
          <Form.Item
            name="email"
            rules={[
              {
                required: "true",
                message: "Email không được bỏ trống",
              },
              {
                type: "email",
                message: "Nhập đúng định dạng email",
              },
            ]}
          >
            <Input
              prefix={<MailOutlined className="mr-2" />}
              placeholder="Email"
              size="large"
            />
          </Form.Item>
          <Form.Item
            name="soDt"
            rules={[
              {
                required: "true",
                message: "Email không được bỏ trống",
              },
              {
                pattern: regex.phone,
                message: "Nhập đúng định dạng số điện thoại",
              },
            ]}
          >
            <Input
              prefix={<PhoneOutlined className="mr-2" />}
              placeholder="Số điện thoại"
              size="large"
            />
          </Form.Item>
          <Form.Item
            name="maLoaiNguoiDung"
            rules={[
              {
                required: "true",
                message: "Vui lòng chọn loại người dùng",
              },
            ]}
          >
            <Select placeholder="Loại người dùng" size="large">
              <Select.Option value="GV">Giáo Vụ</Select.Option>
              <Select.Option value="HV">Học Viên</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item
            wrapperCol={{
              offset: 10,
              span: 5,
            }}
          >
            <Button
              form="form_add_cource"
              htmlType="submit"
              size="large"
              className="text-white bg-green-400"
            >
              {" "}
              Cập nhật
            </Button>
          </Form.Item>
        </Form>
      </Modal>
      {/* modal thêm người dùng */}
      <Modal
        width={600}
        className="modal"
        open={isModalOpen2}
        onCancel={handleCancel2}
      >
        <p className="text-2xl text-white text-center py-2 uppercase">
          Thêm người dùng
        </p>
        <Form onFinish={handelAddUser}>
          <Form.Item
            name="taiKhoan"
            rules={[
              {
                required: "true",
                message: "Tài khoản không được bỏ trống",
              },
              {
                pattern: regex.user,
                message:
                  "Tài khoản gồm 6-12 ký tự,không khoảng cách, không ký tự đặc biệt ",
              },
            ]}
          >
            <Input
              placeholder="Tài khoản"
              prefix={<SmileOutlined className="mr-2" />}
              size="large"
            />
          </Form.Item>
          <Form.Item
            name="matKhau"
            rules={[
              {
                required: "true",
                message: "Mật khẩu không được bỏ trống",
              },
              {
                pattern: regex.password,
                message:
                  "Mật khẩu gồm 8-15 ký tự, 1 ký tự viết hoa, 1 số, 1 ký tự đặc biệt",
              },
            ]}
          >
            <Input.Password
              prefix={<LockOutlined className="mr-2" />}
              placeholder="Mật Khẩu"
              size="large"
            />
          </Form.Item>
          <Form.Item
            name="hoTen"
            rules={[
              {
                required: "true",
                message: "Họ tên không được bỏ trống",
              },
            ]}
          >
            <Input
              prefix={<UserOutlined className="mr-2" />}
              placeholder="Họ và tên"
              size="large"
            />
          </Form.Item>
          <Form.Item
            name="email"
            rules={[
              {
                required: "true",
                message: "Email không được bỏ trống",
              },
              {
                type: "email",
                message: "Nhập đúng định dạng email",
              },
            ]}
          >
            <Input
              prefix={<MailOutlined className="mr-2" />}
              placeholder="Email"
              size="large"
            />
          </Form.Item>
          <Form.Item
            name="soDt"
            rules={[
              {
                required: "true",
                message: "Email không được bỏ trống",
              },
              {
                pattern: regex.phone,
                message: "Nhập đúng định dạng số điện thoại",
              },
            ]}
          >
            <Input
              prefix={<PhoneOutlined className="mr-2" />}
              placeholder="Số điện thoại"
              size="large"
            />
          </Form.Item>
          <Form.Item
            name="maLoaiNguoiDung"
            rules={[
              {
                required: "true",
                message: "Vui lòng chọn loại người dùng",
              },
            ]}
          >
            <Select placeholder="Loại người dùng" size="large">
              <Select.Option value="GV">Giáo Vụ</Select.Option>
              <Select.Option value="HV">Học Viên</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item
            wrapperCol={{
              offset: 10,
              span: 5,
            }}
          >
            <Button
              htmlType="submit"
              size="large"
              className="text-white bg-green-400"
            >
              {" "}
              Xác Nhận
            </Button>
          </Form.Item>
        </Form>
      </Modal>
      {/* modal ghi danh */}
      <Modal
        width={600}
        className="modal text-xl"
        open={isModalOpen3}
        onCancel={handleCancel3}
      >
        <div className=" grid px-2 bg-white grid-cols-6 border-none text-center rounded-lg mb-3 uppercase border-b-2 ">
          <h6 className="text-lg pt-3 col-span-2 font-medium">chọn khóa học</h6>
          <Form
            onFinish={handelAdRegis}
            className=" col-span-4 flex  items-center justify-between"
          >
            <Form.Item
              className="w-3/4 pt-3"
              name="maKhoaHoc"
              wrapperCol={{
                offset: 1,
                span: 24,
              }}
            >
              <Select
                className="font-bold text-3xl"
                placeholder="Chọn khóa học"
              >
                {listRegisterCourses.map((item, index) => {
                  return (
                    <Select.Option value={item.maKhoaHoc} key={index}>
                      <span className="text-lg">{item.tenKhoaHoc}</span>
                    </Select.Option>
                  );
                })}
              </Select>
            </Form.Item>
            <Form.Item
              className="pt-3"
              wrapperCol={{
                offset: 1,
                span: 24,
              }}
            >
              <Button
                htmlType="submit"
                className="bg-green-500 mr-2  flex items-center justify-center rounded px-2 py-1 text-white font-medium  text-lg hover:bg-green-700 duration-300"
              >
                <span className="">Ghi danh</span>
              </Button>
            </Form.Item>
          </Form>
        </div>
        <div className="py-2">
          <h6 className="text-xl font-bold text-white">
            Khóa học chờ xác thực
          </h6>
          <Table
            dataSource={listPendingRegistedCourses}
            columns={columsKhChoXacThuc}
            pagination={{
              pageSize: 2,
              position: ["bottomCenter"],
            }}
          />
        </div>
        <div className="py-2">
          <h6 className="text-xl font-bold text-white">Khóa học đã ghi danh</h6>
          <Table
            columns={columsKhDaGhiDanh}
            dataSource={listRegistedCourses}
            pagination={{
              pageSize: 2,
              position: ["bottomCenter"],
            }}
          />
        </div>
      </Modal>
      {/* modal cập nhập thông tin giáo vụ  */}
      <Modal className="modal" open={isModalOpen4} onCancel={handleCancel4}>
        <p className="text-2xl text-white text-center py-2 uppercase">
          thông tin cá nhân
        </p>
        <Form autoComplete="off" name="form_add_cource" onFinish={updateInfo}>
          <Form.Item initialValue={user.taiKhoan} name="taiKhoan">
            <Input
              disabled
              prefix={<SmileOutlined className="mr-2" />}
              size="large"
            />
          </Form.Item>
          <Form.Item
            name="matKhau"
            rules={[
              {
                required: "true",
                message: "Mật khẩu không được bỏ trống",
              },
              {
                pattern: regex.password,
                message:
                  "Mật khẩu gồm 8-15 ký tự, 1 ký tự viết hoa, 1 số, 1 ký tự đặc biệt",
              },
            ]}
          >
            <Input.Password
              prefix={<LockOutlined className="mr-2" />}
              placeholder="Mật Khẩu"
              size="large"
            />
          </Form.Item>
          <Form.Item
            initialValue={user.hoTen}
            name="hoTen"
            rules={[
              {
                required: "true",
                message: "Họ tên không được bỏ trống",
              },
            ]}
          >
            <Input
              prefix={<UserOutlined className="mr-2" />}
              placeholder="Họ và tên"
              size="large"
            />
          </Form.Item>
          <Form.Item
            initialValue={user.email}
            name="email"
            rules={[
              {
                required: "true",
                message: "Email không được bỏ trống",
              },
              {
                type: "email",
                message: "Nhập đúng định dạng email",
              },
            ]}
          >
            <Input
              prefix={<MailOutlined className="mr-2" />}
              placeholder="Email"
              size="large"
            />
          </Form.Item>
          <Form.Item
            initialValue={user.soDT}
            name="soDt"
            rules={[
              {
                required: "true",
                message: "Email không được bỏ trống",
              },
              {
                pattern: regex.phone,
                message: "Nhập đúng định dạng số điện thoại",
              },
            ]}
          >
            <Input
              prefix={<PhoneOutlined className="mr-2" />}
              placeholder="Số điện thoại"
              size="large"
            />
          </Form.Item>
          <Form.Item
            name="maLoaiNguoiDung"
            rules={[
              {
                required: "true",
                message: "Vui lòng chọn loại người dùng",
              },
            ]}
          >
            <Select placeholder="Loại người dùng" size="large">
              <Select.Option value="GV">Giáo Vụ</Select.Option>
              <Select.Option value="HV">Học Viên</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item
            wrapperCol={{
              offset: 10,
              span: 5,
            }}
          >
            <Button
              form="form_add_cource"
              htmlType="submit"
              size="large"
              className="text-white bg-green-400"
            >
              {" "}
              Cập nhật
            </Button>
          </Form.Item>
        </Form>
      </Modal>
      {/*  */}
      <div className="containerbg break-all overflow-y-scroll">
        <div className="dashboard p-5">
          <div className="detail card p-5">
            <div className="cardHeader px-5">
              <div className="flex justify-around items-center">
                <div className="button">
                  <button
                    onClick={showModal2}
                    className="bg-green-500 rounded py-2 text-white font-medium px-4 text-lg hover:bg-green-700 duration-300"
                  >
                    Thêm người dùng
                  </button>
                </div>
                <div className="search flex justify-between">
                  <form action="">
                    <input
                      onChange={handelOnSearch}
                      className="form-control"
                      type="text"
                      placeholder="Nhập vào tài khoản hoặc họ tên người dùng"
                    />
                  </form>
                </div>
                <div className="dropdown">
                  <button onClick={handelToggle}>
                    <MenuOutlined />
                  </button>
                  <div
                    className={
                      show
                        ? `on dropdown-content-admin  `
                        : "dropdown-content-admin"
                    }
                  >
                    <ul>
                      <li className="p-2">
                        <button className="text-sm">
                          <NavLink to="/">
                            <HomeOutlined className="mr-2" />
                            Trang Chủ
                          </NavLink>
                        </button>
                      </li>
                      <li className="p-2 text-sm">
                        <button className="text-sm" onClick={showModal4}>
                          <i className="fa fa-user-circle mr-2"></i>
                          Cập nhập thông tin
                        </button>
                      </li>
                      <li className="text-sm p-2">
                        <button>
                          <NavLink to="/admin/quanlykhoahoc">
                            <RadarChartOutlined className="mr-2" />
                            Quản lý khóa học
                          </NavLink>
                        </button>
                      </li>
                      <li className="p-2 text-sm">
                        <button
                          onClick={() => {
                            LocalService.removeItem("USER_INFO");

                            window.location.reload();
                          }}
                        >
                          <i className="mr-2 fa fa-sign-out-alt"></i>
                          Đăng xuất
                        </button>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div className="table p-5">
              <Table
                size="large
            "
                pagination={{
                  pageSize: 6,
                  position: ["bottomCenter"],
                }}
                dataSource={dataSource && dataSource}
                columns={dataSource && columns}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
