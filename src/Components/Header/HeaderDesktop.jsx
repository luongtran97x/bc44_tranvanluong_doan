  import { Form, Input } from "antd";
import React, { useEffect, useRef, useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import { Menu } from "antd";
import { MenuOutlined, SettingOutlined } from "@ant-design/icons";
import "./Header.css";
import ScrollToTop from "react-scroll-to-top";
import { LocalService } from "../../services/LocalService";
import { useSelector } from "react-redux";

const items = [
  {
    label: "DANH MỤC",
    key: "danhmuc",
    icon: <MenuOutlined />,
    style: {
      borderColor: "none",
      border: "none",
    },
    theme: "dark",
    children: [
      {
        label: (
          <NavLink to="/danhmuckhoahoc/BackEnd">LẬP TRÌNH BACKEND</NavLink>
        ),
        key: "backend",
      },
      {
        label: <NavLink to="/danhmuckhoahoc/Design">THIẾT KẾ WEB</NavLink>,
        key: "design",
      },
      {
        label: (
          <NavLink to="/danhmuckhoahoc/DiDong">LẬP TRÌNH DI ĐỘNG </NavLink>
        ),
        key: "diDong",
      },
      {
        label: (
          <NavLink to="/danhmuckhoahoc/FrontEnd">LẬP TRÌNH FONTEND</NavLink>
        ),
        key: "frontEnd",
      },
      {
        label: (
          <NavLink to="/danhmuckhoahoc/FullStack">LẬP TRÌNH FULLSTACK</NavLink>
        ),
        key: "fullStack",
      },
      {
        label: <NavLink to="/danhmuckhoahoc/TuDuy">LẬP TRÌNH TƯ DUY</NavLink>,
        key: "tuDuy",
      },
    ],
  },
  {
    label: <NavLink to="/khoahoc">KHÓA HỌC</NavLink>,
    key: "khoaHoc",
  },
  {
    label: <NavLink to="/blog">BLOG</NavLink>,
    key: "blog",
  },
  {
    label: "SỰ KIỆN",
    style: { transition: "none" },
    children: [
      {
        label: <NavLink to="/login">SỰ KIỆN CUỐI NĂM</NavLink>,
      },
      {
        label: <NavLink to="/login">SỰ KIỆN GIÁNG SINH</NavLink>,
      },
      {
        label: <NavLink to="/login">SỰ KIỆN GIÁNG NOEL</NavLink>,
      },
    ],
  },
  {
    label: <NavLink to="/news"> THÔNG TIN </NavLink>,
  },
];

export default function HeaderDeskTop() {
  const navigate = useNavigate();
  const toggleRef = useRef(null);
  const [show, setShow] = useState(false);
  const [headerFixed, setHeaderFixed] = useState(false);
  const user = useSelector((state) => state.userSlice.userInfo);

  //  Xử lý move to top
  useEffect(() => {
    const handleScroll = () => {
      if (window.pageYOffset > 50) {
        setHeaderFixed(true);
      } else {
        setHeaderFixed(false);
      }
    };
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);
  // Xử lý dropdown
  useEffect(() => {
    const handelClickOutSide = (event) => {
      if (toggleRef.current && !toggleRef.current.contains(event.target)) {
        setShow(false);
      }
    };
    document.addEventListener("click", handelClickOutSide);
    return () => {
      document.removeEventListener("click", handelClickOutSide);
    };
  }, []);

  // Xử lý chức năng tìm kiếm
  const handelOnSearch = (values) => {
    navigate(`/search/${values}`);
  };
  // xử lý dropdown
  const handelToggle = () => {
    setShow(!show);
  };
  const renderButton = () => {
    if (user && user.maLoaiNguoiDung === "HV") {
      return (
        <div ref={toggleRef} className="icon">
          <button onClick={handelToggle}>
            <img
              className="w-12 h-12 object-cover rounded-full"
              src="https://cdn.dribbble.com/users/2364329/screenshots/6676961/02.jpg?compress=1&resize=800x600"
              alt="img"
            />
          </button>
          <div className={show ? `dropdown-content show ` : "dropdown-content"}>
            <ul>
              <li className="p-2">
                <NavLink to="/userInfo">
                  <i className="mr-2 fa fa-user-edit"></i>
                  Trang cá nhân
                </NavLink>{" "}
              </li>
              <li className="p-2">
                <NavLink to="/history">
                  <i className=" mr-2 fa fa-cog"></i>
                  Lịch sử đăng ký
                </NavLink>
              </li>
              <li className="p-2">
                <button
                  onClick={() => {
                    LocalService.removeItem("USER_INFO");
                    window.location.reload();
                  }}
                >
                  <i className="mr-5 text-lg fa fa-sign-out-alt"></i>
                  Đăng xuất
                </button>
              </li>
            </ul>
          </div>
        </div>
      );
    } else if (user && user.maLoaiNguoiDung === "GV") {
      return (
        <>
          <button className=" text-green-600  ">
            <NavLink to="/admin/quanlynguoidung">
                <SettingOutlined className="block text-xl" />
                <span className="font-medium uppercase">Trang quản trị</span>
            </NavLink>
          </button>
          <div ref={toggleRef} className="icon">
            <button onClick={handelToggle}>
              <img
                className="w-12 h-12 object-cover rounded-full"
                src="https://cdn.dribbble.com/users/2364329/screenshots/6676961/02.jpg?compress=1&resize=800x600"
                alt="img"
              />
            </button>
            <div
              className={show ? `dropdown-content show ` : "dropdown-content"}
            >
              <ul>
                <li className="p-2">
                  <NavLink to="/userInfo">
                    <i className="mr-2 fa fa-user-edit"></i>
                    Trang cá nhân
                  </NavLink>{" "}
                </li>
                <li className="p-2">
                  <NavLink to="/history">
                    <i className=" mr-2 fa fa-cog"></i>
                    Lịch sử đăng ký
                  </NavLink>
                </li>
                <li className="p-2">
                  <button
                    onClick={() => {
                      LocalService.removeItem("USER_INFO");
                      window.location.reload();
                    }}
                  >
                    <i className="mr-5 text-lg fa fa-sign-out-alt"></i>
                    Đăng xuất
                  </button>
                </li>
              </ul>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <button
          style={{
            background: "#f6ba35",
            color: "#fff",
            padding: "5px 10px",
          }}
        >
          <NavLink to="/login">ĐĂNG NHẬP</NavLink>
        </button>
      );
    }
  };

  return (
    <div className={`${headerFixed ? "headerFixed" : ""}`}>
      <div
        className="flex justify-between items-center "
        style={{ padding: "10px  50px" }}
      >
        <div className="headerLeft  flex items-end space-x-3 justify-center">
          <NavLink to="/">
            <img
              src="https://demo2.cybersoft.edu.vn/logo.png"
              alt="logo"
              width={250}
            />
          </NavLink>
          <Form>
            <Form.Item name="search">
              <Input.Search onSearch={handelOnSearch} placeholder="Tìm kiếm khóa học" />
            </Form.Item>
          </Form>
        </div>
        <div className="headerRight">
          <Menu
            style={{
              fontWeight: 700,
              width: "100%",
              border: "none",
            }}
            mode="horizontal"
            items={items}
          />
        </div>
        <div className="headerRight-Login flex space-x-5">{renderButton()}</div>
        <ScrollToTop
          className="flex items-center justify-center bg-green-600 font-medium"
          color="white"
          smooth
        />
      </div>
    </div>
  );
}
