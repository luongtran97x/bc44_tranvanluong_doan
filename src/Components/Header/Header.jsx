
import React from "react";
import { Desktop, Mobile, Tablet } from "../../responsive/responsive";
import HeaderDeskTop from "./HeaderDesktop";
import HeaderTablet from "./HeaderTablet";
import HeaderMobile from "./HeaderMobile";


export default function Header() {
 
  return (
    <>
            <Desktop>
                <HeaderDeskTop/>
            </Desktop>

            <Tablet>
                <HeaderTablet/>
            </Tablet>

            <Mobile>
              <HeaderMobile/>
            </Mobile>
    </>
  );
}
