import React from "react";
import "./Footer.css";
export default function FooterDesktop() {
  return (
    <div style={{ background: "#f0f8ff", padding: "10px" }}>
      <div className="footer-body" style={{ padding: "10px 40px" }}>
        <div className="grid grid-cols-6  textCardTitle">
          <div className="vLearning col-span-2 ">
            <a href="#" className="relative textLogo mr-5">
              <span className="textE font-medium">V</span>
              learning
              <i className="far fa-keyboard iconLogo absolute"></i>
            </a>
            <ul className="menuFooter">
              <li>
                <i className="fas fa-phone-alt iconFooter"></i>
                <span className="font-medium">1800-123-4567</span>
              </li>
              <li>
                <i className="fas fa-envelope-open-text iconFooter"></i>
                <span className="font-medium">devit@gmail.com</span>
              </li>{" "}
              <li>
                <i className="fas fa-map-marker-alt iconFooter"></i>
                <span className="font-medium">Đà Nẵng</span>
              </li>
            </ul>
          </div>
          <div className="link col-span-1">
            <p className="textFooterTitle">Liên kết</p>
            <ul className="menuFooter">
              <li>
                <i className="fas fa-chevron-right">
                </i>
                  Trang chủ
              </li>
              <li>
                <i className="fas fa-chevron-right">
                </i>
                  Dịch vụ
              </li>
              <li>
                <i className="fas fa-chevron-right">
                </i>
                  Nhóm
              </li>
              <li>
                <i className="fas fa-chevron-right">
                </i>
                  Blog
              </li>
            </ul>
          </div>
          <div className="course col-span-1 ">
            <p className="textFooterTitle">Khóa học</p>
            <ul className="menuFooter">
              <li>
                <i className="fas fa-chevron-right">
                </i>
                  Font End
              </li>
              <li>
                <i className="fas fa-chevron-right">
                </i>
                  Back End
              </li>
              <li>
                <i className="fas fa-chevron-right">
                </i>
                  Full stack
              </li>
              <li>
                <i className="fas fa-chevron-right">
                </i>
                  Node Js
              </li>
            </ul>
          </div>
          <div className="col-span-2 text-center">
            <p className="textFooterTitle">Đăng kí tư vấn</p>
            <form action="" className="flex flex-col items-center">
              <input type="text" className="formFooter text-lg" placeholder="Họ và tên" />
              <input type="text" className="formFooter text-lg" placeholder="Email" />
              <input type="text" className="formFooter text-lg" placeholder="Số điện thoại" />
            </form>
            <button className="text-white text-lg font-medium mt-4" style={{background:"#f6ba35", padding:"5px 10px"}}>ĐĂNG KÍ</button>
          </div>
        </div>
      </div>
      <div className="extraFooter flex justify-between" style={{
        borderTop:"1px solid #d0d0d0",
        padding:"5px 50px"
      }}>
        <div className="title">
            <p>Copyright © 2023. All rights reserved.</p>
        </div>
        <div className="social">
          <i className="fab fa-instagram iconFooter iconSize"> </i>
          <i className="fab fa-facebook-f iconFooter iconSize"></i>
          <i className="fab fa-twitter iconFooter iconSize"></i>
        </div>
      </div>
    </div>
  );
}
