import "./App.css";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import HomePage from "./User/Pages/HomePage/HomePage";
import Login from "./User/Pages/Login&Register/Login";
import Layout from "./Layout/Layout";
import ListCourses from "./User/Pages/ListDetailCourses/ListCourses";
import AllCoures from "./User/Pages/AllCourses/AllCoures";
import Blog from "./User/Pages/Blog/Blog";
import News from "./User/Pages/News/News";
import DetailCourses from "./User/Pages/DetailCourses/DetailCourses";
import UserInfo from "./User/Pages/UserInfo/UserInfo";
import History from "./User/Pages/History/History";
import Search from "./User/Pages/Search/Search";
import NotFoundPage from "./User/Pages/NotFoundPage/NotFoundPage";
import { useSelector } from "react-redux";
import UserManagement from "./Admin/UserManagement/UserManagement";
import CoursesManagement from "./Admin/CoursesManagement/CoursesManagement";

function App() {
  let user = useSelector((state) => state.userSlice.userInfo);

  return (
    <BrowserRouter>
      {/* Route User */}
      <Routes>
        <Route path="/" element={<Layout contentPage={<HomePage />} />} />
        <Route path="/login" element={<Login />} />
        <Route
          path="/khoahoc"
          element={<Layout contentPage={<AllCoures />} />}
        />
        <Route
          path="/danhmuckhoahoc/:id"
          element={<Layout contentPage={<ListCourses />} />}
        />
        <Route path="/blog" element={<Layout contentPage={<Blog />} />} />
        <Route path="/news" element={<Layout contentPage={<News />} />} />
        <Route
          path="/detail/:id"
          element={<Layout contentPage={<DetailCourses />} />}
        />
        <Route
          path="/userInfo"
          element={
            user === null ? (
              <Navigate to="/login" />
            ) : (
              <Layout contentPage={<UserInfo />} />
            )
          }
        />
        <Route
          path="/history"
          element={
            user === null ? (
              <Navigate to="/login" />
            ) : (
              <Layout contentPage={<History />} />
            )
          }
        />
        <Route
          path="/search/:key"
          element={<Layout contentPage={<Search />} />}
        />
        <Route path="*" element={<NotFoundPage />} />

        <Route
          path="/admin/quanlynguoidung"
          element={
           user && user.maLoaiNguoiDung === "GV" ? <UserManagement /> : <Navigate to="/" />
          }
        />
        <Route
          path="/admin/quanlykhoahoc"
          element={
            user &&  user.maLoaiNguoiDung === "GV" ? (
              <CoursesManagement />
            ) : (
              <Navigate to="/" />
            )
          }
        />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
