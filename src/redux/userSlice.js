import { createSlice } from "@reduxjs/toolkit";
import { LocalService } from "../services/LocalService";

const initialState = {
  userInfo: LocalService.getItem("USER_INFO"),
  crudUser: {},
};

const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    setLogin: (state, action) => {
      state.userInfo = action.payload;
    },
    setCrudUser: (state, action) => {
      state.crudUser = action.payload;
    },
    updateInfo: (state, action) => {
      state.crudUser = action.payload;
    },
    update:(state,action) => { 
      state.userInfo = action.payload
     }
  },
});

export const { setLogin, updateInfo, setCrudUser ,update} = userSlice.actions;

export default userSlice.reducer;
