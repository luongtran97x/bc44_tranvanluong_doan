import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  userManagement: [],
  listRegisterCourses: [],
  listRegistedCourses: [],
  listPendingRegistedCourses: [],
  userDynamic: {},
};

const userManagement = createSlice({
  name: "userManagement",
  initialState,
  reducers: {
    setUserManagement: (state, action) => {
      state.userManagement = action.payload;
    },
    updateUserManagement: (state, action) => {
      let cloneArr = [...state.userManagement];
      let index = cloneArr.findIndex(
        (user) => user.taiKhoan === action.payload.taiKhoan
      );
      if (index !== -1) {
        cloneArr[index] = action.payload;
      }
      state.userManagement = cloneArr;
    },
    addUserManagement: (state, action) => {
      state.userManagement.push(action.payload);
    },
    delUserManagement: (state, action) => {
      state.userManagement = state.userManagement.filter(
        (user) => user.taiKhoan !== action.payload.taiKhoan
      );
    },
    searchUserManagement: (state, action) => {
      state.userManagement = action.payload;
    },
    setListRegisterCourses: (state, action) => {
      state.listRegisterCourses = action.payload;
    },
    setListRegistedCourses: (state, action) => {
      state.listRegistedCourses = action.payload;
    },
    setListPendingRegistedCourses: (state, action) => {
      state.listPendingRegistedCourses = action.payload;
    },
    setUserDynamic: (state, action) => {
      state.userDynamic = action.payload;
    },
    cancelRegis: (state, action) => {
      state.listRegistedCourses = state.listRegistedCourses.filter(
        (item) => item.maKhoaHoc !== action.payload.maKhoaHoc
      );
    },
    confirmRegis: (state, action) => {
      state.listPendingRegistedCourses =
        state.listPendingRegistedCourses.filter(
          (item) => item.maKhoaHoc !== action.payload.maKhoaHoc
        );
    },
    delConfirmRegis: (state, action) => {
      state.listPendingRegistedCourses =
        state.listPendingRegistedCourses.filter(
          (item) => item.maKhoaHoc !== action.payload.maKhoaHoc
        );
    },
 
  },
});

export const {
  confirmRegis,
  delConfirmRegis,
  setUserDynamic,
  cancelRegis,
  setListRegistedCourses,
  setListPendingRegistedCourses,
  setListRegisterCourses,
  searchUserManagement,
  setUserManagement,
  updateUserManagement,
  delUserManagement,
  addUserManagement,
} = userManagement.actions;

export default userManagement.reducer;
