import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  coursesManagement: [],
  dynamicCourse: {},
  listStudent: [],
  listStudentInAttend: [],
  listStudentPendingAttend: [],
  dynamic: {},
};

const coursesManagementSlice = createSlice({
  name: "coursesManagementSlice",
  initialState,
  reducers: {
    setCoursesManagement: (state, action) => {
      state.coursesManagement = action.payload;
    },
    updateCoursesManagement: (state, action) => {
      let cloneArr = [...state.coursesManagement];
      let index = cloneArr.findIndex(
        (item) => item.maKhoaHoc === action.payload.maKhoaHoc
      );
      if (index !== -1) {
        cloneArr[index] = action.payload;
      }
      state.coursesManagement = cloneArr;
    },
    delCoursesManagement: (state, action) => {
      state.coursesManagement = state.coursesManagement.filter(
        (item) => item.maKhoaHoc !== action.payload.maKhoaHoc
      );
    },
    setDynamicCourse: (state, action) => {
      state.dynamicCourse = action.payload;
    },
    setDynamic: (state, action) => {
      state.dynamic = action.payload;
    },
    searchCoursesManagement: (state, action) => {
      state.coursesManagement = action.payload;
    },
    // học viên đã tham gia khóa học
    setListStudentAttended: (state, action) => {
      state.listStudent = action.payload;
    },
    // học viên chưa tham gia khóa học
    setListStudentInAttend: (state, action) => {
      state.listStudentInAttend = action.payload;
    },
    // học viên đợi xác thực đăng ký khóa học
    setListStudentPendingAttend: (state, action) => {
      state.listStudentPendingAttend = action.payload;
    },
    // xóa học viên đã tham gia khóa học
    delStudentAttended: (state, action) => {
      state.listStudent = state.listStudent.filter(
        (item) => item.taiKhoan !== action.payload.taiKhoan
      );
    },
    // xóa học viên chờ xác thực ghi danh
    delStudentPendingAttend: (state, action) => {
      state.listStudentPendingAttend = state.listStudentPendingAttend.filter(
        (item) => item.taiKhoan !== action.payload.taiKhoan
      );
    },
    // ghi danh sinh viên tham gia khóa học
    confirmStudentAttend: (state,action) => { 
      state.listStudentPendingAttend = state.listStudentPendingAttend.filter(
        (item) => item.taiKhoan !== action.payload.taiKhoan
      );
     },
    //  tạo khóa học
    createCourse:(state,action) => { 
      state.coursesManagement.push(action.payload)
     }
  },
});

export const {
  createCourse,
  confirmStudentAttend,
  delStudentPendingAttend,
  setDynamic,
  delStudentAttended,
  setListStudentPendingAttend,
  setListStudentInAttend,
  setListStudentAttended,
  setCoursesManagement,
  setDynamicCourse,
  updateCoursesManagement,
  delCoursesManagement,
  searchCoursesManagement,
} = coursesManagementSlice.actions;

export default coursesManagementSlice.reducer;
