import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  coursesList: [],
  crudList: [],

};

const courseSlice = createSlice({
  name: "courseSlice",
  initialState,
  reducers: {
    setCourse: (state, action) => {
      state.coursesList = action.payload;
    },
    setListCourse: (state, action) => {
      state.crudList = action.payload;
    },
    removeCourse: (state, action) => {
      state.crudList = state.crudList.filter(
        (item) => item.maKhoaHoc !== action.payload.maKhoaHoc
      );
    },
   

    addCourse: (state, action) => {
      state.crudList.push(action.payload);
    },
   
  },
});

export const { setCourse, setListCourse, removeCourse, addCourse,  } =
  courseSlice.actions;

export default courseSlice.reducer;
