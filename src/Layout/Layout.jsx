import React from "react";
import Footer from "../Components/Footer/Footer";
import Header from "../Components/Header/Header";

export default function Layout({ contentPage }) {
  return (
    <div>
      <Header />
      {contentPage}
      <Footer />
    </div>
  );
}
