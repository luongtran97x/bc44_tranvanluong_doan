import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import userSlice from "./redux/userSlice";
import courseSlice from "./redux/courseSlice";
import { Provider } from "react-redux";
import { configureStore } from "@reduxjs/toolkit";
import userManagementSlice from "./redux/Admin/userManagementSlice";
import coursesManagementSlice from "./redux/Admin/coursesManagementSlice";


export const store = configureStore({
  reducer: {
    userSlice: userSlice,
    courseSlice: courseSlice,
    userManagement:userManagementSlice,
    coursesManagement:coursesManagementSlice,
  },
});
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Provider store={store}>
    <App />
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
