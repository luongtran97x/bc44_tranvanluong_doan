import React from "react";
import "./Blog.css";
export default function () {
  return (
    <div>
      <div
        className="banner"
        style={{ padding: "50px", background: "#ffd60a", color: "white" }}
      >
        <p className="text-3xl font-medium">BLOG </p>
        <p> BLOG THÔNG TIN CÔNG NGHỆ SỐ!!!</p>
      </div>
      <div className="" style={{ padding: " 50px" }}>
        <p className="text-xl font-medium">
          <i
            className="fas fa-bookmark"
            style={{ fontSize: "20px", marginRight: "5px", color: "#ed85ab" }}
          ></i>
          PHÙ HỢP VỚI BẠN
        </p>
      </div>
      <div className="grid xl:grid-cols-6 md:grid-cols-6 sm:grid-cols-none" style={{ padding: "0 50px" }}>
        <div className="blogItemLeft col-span-4">
          <div className="grid xl:grid-cols-2 md:grid-cols-1 sm:grid-cols-1">
            <div className="cardBlog mx-2">
              <div className="cardBlogContent">
                <div className="imgCardBlog">
                  <img src="https://os-system.com/blog/wp-content/uploads/2021/10/1_FDNeKIUeUnf0XdqHmi7nsw.png" alt="" />
                  <p className="font-medium">
                    Material UI custom theme với TypeScript
                  </p>
                </div>
                <div className="timeBlogCourse">
                  <div className="reviewBlog">
                    <span>
                      <i className="far fa-thumbs-up"></i>
                      300
                    </span>
                    <span>
                      <i className="far fa-comment"></i>
                      500
                    </span>
                    <span>
                      <i className="fas fa-eye"></i>
                      200
                    </span>
                  </div>
                  <p>
                    {" "}
                    Đăng bởi <span>Jhony Đặng</span>{" "}
                  </p>
                </div>
                <p className="colorCardTitle">
                  Có lẽ cũng rất lâu rồi mà tôi chưa đụng đến thứ được gọi là
                  "timetable". Hay dân dã hơn thì người ta hay gọi là "Lịch
                  thường nhật",...
                </p>
                <button className="btnGlobal btnCardBlog">
                  <a href="#">Xem thêm</a>
                </button>
              </div>
            </div>
            <div className="cardBlog  mx-2">
              <div className="cardBlogContent">
                <div className="imgCardBlog">
                  <img src="https://images.viblo.asia/1e584999-0b5a-4189-9081-b9a41212265d.jpg" alt="img" />
                  <p className="font-medium">
                    Tailwind css và cách cài đặt cơ bản
                  </p>
                </div>
                <div className="timeBlogCourse">
                  <div className="reviewBlog">
                    <span>
                      <i className="far fa-thumbs-up"></i>
                      300
                    </span>
                    <span>
                      <i className="far fa-comment"></i>
                      500
                    </span>
                    <span>
                      <i className="fas fa-eye"></i>
                      200
                    </span>
                  </div>
                  <p>
                    {" "}
                    Đăng bởi <span>Jhony Đặng</span>{" "}
                  </p>
                </div>
                <p className="colorCardTitle">
                  Có lẽ cũng rất lâu rồi mà tôi chưa đụng đến thứ được gọi là
                  "timetable". Hay dân dã hơn thì người ta hay gọi là "Lịch
                  thường nhật",...
                </p>
                <button className="btnGlobal btnCardBlog">
                  <a href="#">Xem thêm</a>
                </button>
              </div>
            </div>
            <div className="cardBlog mx-2">
              <div className="cardBlogContent">
                <div className="imgCardBlog">
                  <img  src="https://www.vectorlogo.zone/logos/w3_html5/w3_html5-ar21.png" alt="img" />
                  <p className="font-medium">Cấu trúc cơ bản trong HTML</p>
                </div>
                <div className="timeBlogCourse">
                  <div className="reviewBlog">
                    <span>
                      <i className="far fa-thumbs-up"></i>
                      300
                    </span>
                    <span>
                      <i className="far fa-comment"></i>
                      500
                    </span>
                    <span>
                      <i className="fas fa-eye"></i>
                      200
                    </span>
                  </div>
                  <p>
                    {" "}
                    Đăng bởi <span>Jhony Đặng</span>{" "}
                  </p>
                </div>
                <p className="colorCardTitle">
                  Có lẽ cũng rất lâu rồi mà tôi chưa đụng đến thứ được gọi là
                  "timetable". Hay dân dã hơn thì người ta hay gọi là "Lịch
                  thường nhật",...
                </p>
                <button className="btnGlobal btnCardBlog">
                  <a href="#">Xem thêm</a>
                </button>
              </div>
            </div>
            <div className="cardBlog  mx-2">
              <div className="cardBlogContent">
                <div className="imgCardBlog">
                  <img src="https://www.verywellmind.com/thmb/4akZH3LzYcM1I4R08ffLcu11SW0=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/what-is-motivation-2795378_color4-5b61dbeec9e77c0050889e49.png" alt="img" />
                  <p className="font-medium">Thời gian và động lực</p>
                </div>
                <div className="timeBlogCourse">
                  <div className="reviewBlog">
                    <span>
                      <i className="far fa-thumbs-up"></i>
                      300
                    </span>
                    <span>
                      <i className="far fa-comment"></i>
                      500
                    </span>
                    <span>
                      <i className="fas fa-eye"></i>
                      200
                    </span>
                  </div>
                  <p>
                    {" "}
                    Đăng bởi <span>Jhony Đặng</span>{" "}
                  </p>
                </div>
                <p className="colorCardTitle">
                  Chắc hẳn ai cũng biết một trang web thì không thể nào thiếu đi
                  HTML và HTML làm nên cấu trúc của một trang web...
                </p>
                <button className="btnGlobal btnCardBlog">
                  <a href="#">Xem thêm</a>
                </button>
              </div>
            </div>
            <div className="cardBlog mx-2">
              <div className="cardBlogContent">
                <div className="imgCardBlog">
                  <img src="https://phamvantu.com/wp-content/uploads/2023/04/async-await.png" alt="img" />
                  <p className="font-medium">
                    Xử lý bất đồng bộ trong Javascript (phần 2)
                  </p>
                </div>
                <div className="timeBlogCourse">
                  <div className="reviewBlog">
                    <span>
                      <i className="far fa-thumbs-up"></i>
                      300
                    </span>
                    <span>
                      <i className="far fa-comment"></i>
                      500
                    </span>
                    <span>
                      <i className="fas fa-eye"></i>
                      200
                    </span>
                  </div>
                  <p>
                    {" "}
                    Đăng bởi <span>Jhony Đặng</span>{" "}
                  </p>
                </div>
                <p className="colorCardTitle">
                  Async/await là cơ chế giúp bạn thực thi các thao tác bất đồng
                  bộ một cách tuần tự hơn , giúp đoạn code nhìn qua tưởng như
                  đồng...
                </p>
                <button className="btnGlobal btnCardBlog">
                  <a href="#">Xem thêm</a>
                </button>
              </div>
            </div>
            <div className="cardBlog  mx-2">
              <div className="cardBlogContent">
                <div className="imgCardBlog">
                  <img src="https://nodemy.vn/wp-content/uploads/2023/03/type.png" alt="ts" />
                  <p>TypeScript là gì, Vì sao nên dùng TypeScript</p>
                </div>
                <div className="timeBlogCourse">
                  <div className="reviewBlog">
                    <span>
                      <i className="far fa-thumbs-up"></i>
                      300
                    </span>
                    <span>
                      <i className="far fa-comment"></i>
                      500
                    </span>
                    <span>
                      <i className="fas fa-eye"></i>
                      200
                    </span>
                  </div>
                  <p>
                    {" "}
                    Đăng bởi <span>Jhony Đặng</span>{" "}
                  </p>
                </div>
                <p className="colorCardTitle">
                  Link khóa học cho anh em nào tò mò ( Đừng lo vì tất cả đều
                  miễn......)
                </p>
                <button className="btnGlobal btnCardBlog">
                  <a href="#">Xem thêm</a>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className="blogItemRight col-span-2">
          <div className="blogRightBox">
            <p>Các chủ đề được đề xuất</p>
            <ul>
              <li>
                <a href="#">Front-end / Mobile apps</a>
              </li>
              <li>
                <a href="#">UI / UX / Design</a>
              </li>
              <li>
                <a href="#">BACK-END</a>
              </li>
              <li>
                <a href="#">Thư viện</a>
              </li>
              <li>
                <a href="#">Chia sẻ người trong nghề</a>
              </li>
              <li>
                <a href="#">Châm ngôn IT</a>
              </li>
              <li>
                <a href="#">Chủ đề khác</a>
              </li>
            </ul>
          </div>
          <div className="blogRightBox">
            <p>Bài đăng được đề xuất</p>
            <div className="postBlog">
              <img
                src="https://reactrouter.com/ogimage.png"
                alt="img"
              />
              <p className="font-medium">Routing trong reactjs</p>
              <p className="colorCardTitle">
                Chúng ta sẽ cùng nhau tìm hiểu cách routing trong reactjs...
              </p>
              <div className="imgPost flex space-x-2">
                <img
                  src="https://demo2.cybersoft.edu.vn/static/media/instrutor13.0159beae.jpg"
                  alt="img"
                />
                <span className="colorCardTitle"> Nguyên Văn</span>
              </div>
            </div>
            <div className="postBlog">
              <img
                src="https://dmitripavlutin.com/static/640b840cfaf5a5f28958ef3d102b87e9/59014/cover-4.png"
                alt="img"
              />
              <p  className="font-medium">Xử Lý Bất Đồng Bộ Trong Javascript</p>
              <p className="colorCardTitle">
                Chắc chắn khi lập trình, bạn sẽ có các công việc cần thời gian
                delay (gọi API, lấy dữ liệu từ Database, đọc/ghi file,...). Và
                đây...
              </p>
              <div className="imgPost flex space-x-2">
                <img
                  src="https://demo2.cybersoft.edu.vn/static/media/instrutor11.0387fe65.jpg"
                  alt="img"
                />
                <span className="colorCardTitle"> Nguyên Minh</span>
              </div>
            </div>
            <div className="postBlog">
              <img
                src="https://phambinh.net/wp-content/uploads/2019/12/1_XlnVJQ392USdQuQcdyvJ4w.png"
                alt="img"
              />
              <p  className="font-medium">Lập trình hướng đối tượng oop</p>
              <p className="colorCardTitle">
                Chúng ta sẽ cùng nhau tìm hiểu cách oop trong reactjs...
              </p>
              <div className="imgPost flex space-x-2 ">
                <img
                  className="rounded"
                  src="https://demo2.cybersoft.edu.vn/static/media/instrutor12.90a80820.jpg"
                  alt="img"
                />
                <span className="colorCardTitle"> Nguyên Văn Vũ</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
