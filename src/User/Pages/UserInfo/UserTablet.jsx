import React from "react";
import "./UserInfo.css";
import { useEffect } from "react";
import { https } from "../../../services/config";
import { useState } from "react";
import { Button, Form, Input, Modal, message } from "antd";
import { regex } from "../../../Constant/regex";
import { LocalService } from "../../../services/LocalService";
import { useDispatch, useSelector } from "react-redux";
import { setCrudUser, updateInfo } from "../../../redux/userSlice";
import { useNavigate } from "react-router-dom";
export default function UserTablet() {
  const user = LocalService.getItem("USER_INFO");
  console.log("🚀 ~ user:", user);
  const crudUser = useSelector((state) => state.userSlice.crudUser);
  const dispatch = useDispatch();
  const [userInfo, setUserInfo] = useState({});
  const navigate = useNavigate();
  const [isModalOpen, setIsModalOpen] = useState(false);
  useEffect(() => {
    let local = LocalService.getItem("USER_INFO");
    let config = {
      headers: {
        Authorization: `Bearer ${local.accessToken}`,
      },
    };
    https
      .post("api/QuanLyNguoiDung/ThongTinTaiKhoan", null, config)
      .then((res) => {
        setUserInfo(res.data);
        dispatch(setCrudUser(res.data));
      })
      .catch((err) => {});
  }, []);

  // xử lý ngăn người dùng vào trang khi chưa login
  useEffect(() => {
    let token = LocalService.getItem("USER_INFO");
    if (!token) {
      navigate("/");
    }
  }, []);

  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  //  xử lý cập nhập thông tin tài khoản
  const onFinish = (value) => {
    const data = {
      ...value,
      maNhom: userInfo.maNhom,
      taiKhoan: userInfo.taiKhoan,
      maLoaiNguoiDung: userInfo.maLoaiNguoiDung,
    };

    let local = LocalService.getItem("USER_INFO");
    let config = {
      headers: {
        Authorization: `Bearer ${local&& local.accessToken}`,
      },
    };
    https
      .put("api/QuanLyNguoiDung/CapNhatThongTinNguoiDung", data, config)
      .then((res) => {
        message.success("Cập nhật thông tin cá nhân thành công");
        dispatch(updateInfo(data));
        setTimeout(() => {
          handleCancel();
        }, 1000);
      })
      .catch((err) => {
        message.success("Cập nhật thông tin cá nhân thất bại");
      });
  };

  return (
    <div className="h-full">
      <div
        className="banner"
        style={{ padding: "50px", background: "#ffd60a", color: "white" }}
      >
        <p className="text-3xl font-medium">THÔNG TIN CÁ NHÂN </p>
        <p> THÔNG TIN HỌC VIÊN</p>
      </div>
      <div className="infoPageContent md:min-h-full flex flex-col">
        <div
          className="grid xl:grid-cols-3 md:grid-cols-3 sm:grid-cols-0 "
          style={{ height: "640px" }}
        >
          <div className="infoLeft">
            <img
              src="https://cdn.dribbble.com/users/2364329/screenshots/6676961/02.jpg?compress=1&resize=800x600"
              alt="img"
            />
            <h6 className="font-medium">{user.hoTen}</h6>
            <p>Lập trình viên Front-end</p>
            <button className="btnInfo">Hồ sơ cá nhân</button>
          </div>
          <div className="infoCenter">
            <h6 className="font-medium text-xl">THÔNG TIN CÁC NHÂN</h6>
            <p className="font-medium">
              Email : <span className="font-thin">{crudUser.email}</span>{" "}
            </p>
            <p className="font-medium">
              Họ và Tên : <span className="font-thin">{crudUser.hoTen}</span>{" "}
            </p>
            <p className="font-medium">
              Số điện thoại : <span className="font-thin">{crudUser.soDT}</span>{" "}
            </p>
          </div>
          <div className="infoRight">
            <p className="font-medium">
              Tài Khoản: <span className="font-thin">{crudUser.taiKhoan}</span>{" "}
            </p>
            <p className="font-medium">
              Nhóm: <span className="font-thin">{crudUser.maNhom}</span>{" "}
            </p>
            <p className="font-medium">
              Đối tượng:{" "}
              <span className="font-thin">{`${
                crudUser.maLoaiNguoiDung === "HV" ? "Học Viên" : "Giáo Vụ"
              }`}</span>
            </p>
            <button
              onClick={showModal}
              className="px-4 py-2 rounded font-medium text-white hover:bg-orange-400 bg-orange-500"
            >
              CẬP NHẬT
            </button>
          </div>
        </div>
        {/* modal */}
        <Modal className="modal" open={isModalOpen} onCancel={handleCancel}>
          <p className="text-2xl text-white text-center py-2 uppercase">
            thông tin cá nhân
          </p>
          <Form onFinish={onFinish}>
            <Form.Item
              name="matKhau"
              rules={[
                {
                  required: "true",
                  message: "Mật khẩu không được bỏ trống",
                },
                {
                  pattern: regex.password,
                  message:
                    "Mật khẩu gồm 8-15 ký tự, 1 ký tự viết hoa, 1 số, 1 ký tự đặc biệt",
                },
              ]}
            >
              <Input.Password placeholder="Mật Khẩu" size="large" />
            </Form.Item>
            <Form.Item
              name="hoTen"
              rules={[
                {
                  required: "true",
                  message: "Họ tên không được bỏ trống",
                },
              ]}
            >
              <Input placeholder="Họ và tên" size="large" />
            </Form.Item>
            <Form.Item
              name="email"
              rules={[
                {
                  required: "true",
                  message: "Email không được bỏ trống",
                },
                {
                  type: "email",
                  message: "Nhập đúng định dạng email",
                },
              ]}
            >
              <Input placeholder="Email" size="large" />
            </Form.Item>
            <Form.Item
              name="soDT"
              rules={[
                {
                  required: "true",
                  message: "Email không được bỏ trống",
                },
                {
                  pattern: regex.phone,
                  message: "Nhập đúng định dạng số điện thoại",
                },
              ]}
            >
              <Input placeholder="Số điện thoại" size="large" />
            </Form.Item>
            <Form.Item
              wrapperCol={{
                offset: 10,
                span: 5,
              }}
            >
              <Button
                htmlType="submit"
                size="large"
                className="text-white bg-green-400"
              >
                {" "}
                Cập nhật
              </Button>
            </Form.Item>
          </Form>
        </Modal>
      </div>
    </div>
  );
}
