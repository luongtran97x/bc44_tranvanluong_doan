import React from "react";

import { Desktop, Mobile, Tablet } from "../../../responsive/responsive";
import UserDesktop from "./UserDesktop";
import UserTablet from "./UserTablet";
import UserMobile from "./UserMobile";
export default function UserInfo() {
 

  return (
  <>
  
   
              <Desktop>
                  <UserDesktop/>
              </Desktop>

              <Tablet>
                <UserTablet/>
              </Tablet>

              <Mobile>
                <UserMobile/>
              </Mobile>
    </>
  );
}
