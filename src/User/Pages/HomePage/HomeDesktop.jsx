import React from "react";
import "./Home.css";
import IntroCourse from "./IntroCourse/IntroCourse";
import InfoCourse from "./InfoCourse/InfoCourse";
import BoxNumber from "./BoxNumber/BoxNumber";
import Lecturer from "./Lecturer/Lecturer";
import Student from "./Student/Student";
export default function HomeDesktop() {
  return (
    <>
      <div
        style={{
          padding: "0 50px",
          backgroundSize: "cover",
          backgroundRepeat: "no-repeat",
          paddingBottom: "50px",
        }}
      >
        <div className="flex flex-wrap">
          <div className="sloganBox  w-1/2 ">
            <div className="triangleTopRight"></div>
            <div className="smallBox smallboxLeftTop"></div>
            <div className="smallBox smallboxRightTop"></div>
            <div className="smallBox smallboxRightBottom"></div>
            <div className="smallBox smallboxRightBottom doubleBox"></div>
            <div className="sloganContainer">
              <div className="">
                <img
                  src="https://demo2.cybersoft.edu.vn/static/media/paper_plane.93dfdbf5.png"
                  alt="img"
                  className="sliderPlaneImg"
                />
              </div>
              <p>Chào mừng</p>
              <p>đến với môi trường</p>
              <p>
                V<span>learning</span>
              </p>
              <button
                style={{
                  padding: "10px 10px",
                  background: "#f6ba35",
                  fontSize: "15px",
                  color: "#fff",
                }}
              >
                BẮT ĐẦU NÀO
              </button>
            </div>
          </div>
          <div className="w-1/2">
            <div className="sliderRight relative">
              <div className="">
                <img
                  className="sliderMainImg"
                  src="https://demo2.cybersoft.edu.vn/static/media/slider2.f170197b.png"
                  alt="img"
                />
                <img
                  className="sliderSubImg sliderCodeImg"
                  src="https://demo2.cybersoft.edu.vn/static/media/code_slider.8c12bbb4.png"
                  alt="img"
                />
                <img
                  className="sliderSubImg sliderMesImg "
                  src="https://demo2.cybersoft.edu.vn/static/media/message_slider.6835c478.png"
                  alt="img"
                />
                <img
                  className="sliderSubImg sliderCloudImg"
                  src="https://demo2.cybersoft.edu.vn/static/media/clouds.15eb556c.png"
                  alt="img"
                />
                <img
                  className="sliderSubImg sliderCloud2Img"
                  src="https://demo2.cybersoft.edu.vn/static/media/clouds.15eb556c.png"
                  alt="img"
                />
                <img
                  className="sliderSubImg sliderCloud3Img"
                  src="https://demo2.cybersoft.edu.vn/static/media/clouds.15eb556c.png"
                  alt="img"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <IntroCourse />
      <InfoCourse />
      <BoxNumber />
      <Lecturer />
      <Student />
    </>
  );
}
