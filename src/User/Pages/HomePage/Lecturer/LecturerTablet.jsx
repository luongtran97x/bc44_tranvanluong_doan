import { Carousel } from "antd";
import React from "react";
import "./Lecturer.css";
export default function LecturerTablet() {
  return (
    <div style={{ padding: "50px" }}>
      <p className="font-bold pb-5">Giảng Viên Hàng Đầu</p>
      <Carousel
        dots={2}
        style={{height:'30vh'}}
        dotPosition="bottom"
        slidesToScroll={1}
        slidesToShow={5}
      >
        <div className="p-3">
          <div
            className="intructorContent "
          >
              <img
                style={{
                  width: "80px",
                  height: "80px",
                  objectFit: "cover",
                  borderRadius: "50%",
                }}
                src="https://demo2.cybersoft.edu.vn/static/media/instrutor5.2e4bd1e6.jpg"
                alt="img"
              />
              <p className="text-xl font-bold">Big Daddy</p>
              <div className="text-black text-md py-2">
                <p>
                  Chuyên gia lĩnh vực <br /> lập trình
                </p>
              </div>
              <p className="rating ">
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <span>4.9</span>
              </p>
              <span style={{fontSize:"13px",color:"#8c8c8c",paddingTop:"5px"}}>100 Đánh giá</span>
          </div>
        </div>
        <div className="p-3">
          <div
            className="intructorContent  "
          >
              <img
                style={{
                  width: "80px",
                  height: "80px",
                  objectFit: "cover",
                  borderRadius: "50%",
                }}
                src="https://demo2.cybersoft.edu.vn/static/media/instrutor6.64041dca.jpg"
                alt="img"
              />
              <p className="text-xl font-bold">Bray</p>
              <div className="text-black text-md py-2">
                <p>
                  Chuyên gia lĩnh vực <br /> Vue Js
                </p>
              </div>
              <p className="rating ">
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <span>4.9</span>
              </p>
              <span style={{fontSize:"13px",color:"#8c8c8c",paddingTop:"5px"}}>100 Đánh giá</span>
          </div>
        </div>
        <div className="p-3">
          <div
            className="intructorContent "
          >
              <img
                style={{
                  width: "80px",
                  height: "80px",
                  objectFit: "cover",
                  borderRadius: "50%",
                }}
                src="https://demo2.cybersoft.edu.vn/static/media/instrutor7.edd00a03.jpg"
                alt="img"
              />
              <p className="text-xl font-bold">WoWy</p>
              <div className="text-black text-md py-2">
                <p>
                  Chuyên gia hệ thống <br /> máy tính
                </p>
              </div>
              <p className="rating ">
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <span>4.9</span>
              </p>
              <span style={{fontSize:"13px",color:"#8c8c8c",paddingTop:"5px"}}>100 Đánh giá</span>
          </div>
        </div>
        <div className="p-3">
          <div
            className="intructorContent  "
          >
              <img
                style={{
                  width: "80px",
                  height: "80px",
                  objectFit: "cover",
                  borderRadius: "50%",
                }}
                src="https://demo2.cybersoft.edu.vn/static/media/instrutor8.aec2f526.jpg"
                alt="img"
              />
              <p className="text-xl font-bold">Mohamed</p>
              <div className="text-black text-md py-2">
                <p>
                  Chuyên gia lĩnh vực <br /> ReactJs
                </p>
              </div>
              <p className="rating ">
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <span>4.9</span>
              </p>
              <span style={{fontSize:"13px",color:"#8c8c8c",paddingTop:"5px"}}>100 Đánh giá</span>
          </div>
        </div>
        <div className="p-3">
          <div
            className="intructorContent  "
          >
              <img
                style={{
                  width: "80px",
                  height: "80px",
                  objectFit: "cover",
                  borderRadius: "50%",
                }}
                src="https://demo2.cybersoft.edu.vn/static/media/instrutor10.89946c43.jpg"
                alt="img"
              />
              <p className="text-xl font-bold">Sol7</p>
              <div className="text-black text-md py-2">
                <p>
                  Chuyên gia lĩnh vực <br /> PHP
                </p>
              </div>
              <p className="rating ">
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <span>4.9</span>
              </p>
              <span style={{fontSize:"13px",color:"#8c8c8c",paddingTop:"5px"}}>100 Đánh giá</span>
          </div>
        </div>
        <div className="p-3">
          <div
            className="intructorContent  "
          >
              <img
                style={{
                  width: "80px",
                  height: "80px",
                  objectFit: "cover",
                  borderRadius: "50%",
                }}
                src="https://demo2.cybersoft.edu.vn/static/media/instrutor11.0387fe65.jpg"
                alt="img"
              />
              <p className="text-xl font-bold">Suboi</p>
              <div className="text-black text-md py-2">
                <p>
                  Chuyên gia lĩnh vực <br /> PHP
                </p>
              </div>
              <p className="rating ">
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <span>4.9</span>
              </p>
              <span style={{fontSize:"13px",color:"#8c8c8c",paddingTop:"5px"}}>100 Đánh giá</span>
          </div>
        </div>
      </Carousel>
    </div>
  );
}
