import React from "react";

import { Desktop, Mobile, Tablet } from "../../../../responsive/responsive.js";
import LecturerMobile from "./LecturerMobile";
import LecturerDesktop from "./LecturerDesktop";
import LecturerTablet from "./LecturerTablet";
export default function Lecturer() {
  return (
    <>
      <Desktop>
        <LecturerDesktop />
      </Desktop>
      <Tablet>
        <LecturerTablet />
      </Tablet>
      <Mobile>
        <LecturerMobile />
      </Mobile>
    </>
  );
}
