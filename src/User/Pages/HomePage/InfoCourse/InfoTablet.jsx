import React, { useEffect } from "react";
import { NavLink } from "react-router-dom";
import { https } from "../../../../services/config";
import { useDispatch, useSelector } from "react-redux";
import { setCourse } from "../../../../redux/courseSlice";
import "./InfoCourse.css";
export default function InfoTablet() {
  const courseList = useSelector((state) => state.courseSlice.coursesList);
  const dispatch = useDispatch();
  useEffect(() => {
    https
      .get("api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?MaNhom=GP01")
      .then((res) => {
        dispatch(setCourse(res.data));
      })
      .catch((err) => {
      });
  }, []);

  const renderKhPhoBien = () => {
    if (courseList !== null) {
      return courseList.slice(0, 4).map((item) => {
        return (
          <div key={item.maKhoaHoc} className="item itemEffect cardGlobalRes pb-3">
            <NavLink to={`/detail/${item.maKhoaHoc}`} className="cardGlobal">
              <img
                src={item.hinhAnh}
                style={{ height: "185px", minHeight: "185px" }}
                alt={item.biDanh}
              />
              <span className="stickerCard">
                {item.danhMucKhoaHoc.tenDanhMucKhoaHoc}
              </span>
              <div className="cardBodyGolbal">
                <p className="font-medium">
                  {item.moTa.substring(0, 40) + "..."}
                </p>
                <div className="titleMark">
                  <div className="imgCardFooter">
                    <img
                      src="https://demo2.cybersoft.edu.vn/static/media/avatar2.bb9626e2.png"
                      className="w-full"
                      alt="hinhAnh"
                    />
                  </div>
                  <span style={{ color: "#8c8c8c", marginLeft: "2px" }}>
                    Elon Musk
                  </span>
                </div>
              </div>
              <div
                className="cardFotter flex justify-between items-center"
                style={{
                  padding: "5px 20px",
                  borderTop: "1px solid rgba(0,0,0,.125)",
                }}
              >
                <div className="">
                  <p
                    style={{
                      fontSize: "12px",
                      color: "#8c8c8c",
                      textDecorationLine: "line-through",
                    }}
                  >
                    800.000
                    <sup
                      className="relative"
                      style={{
                        fontSize: "75%",
                        lineHeight: "0",
                        verticalAlign: "baseline",
                        top: "-0.5em",
                      }}
                    >
                      đ
                    </sup>
                  </p>
                  <p style={{ fontWeight: "500", color: "#41b294" }}>
                    400.000
                    <sup
                      className="relative"
                      style={{
                        fontSize: "75%",
                        lineHeight: "0",
                        verticalAlign: "baseline",
                        top: "-0.5em",
                      }}
                    >
                      đ
                    </sup>
                  </p>
                </div>
                <div className="">
                  <i
                    className="fas fa-star mr-1 textStar"
                    style={{ color: "#f6ba35" }}
                  ></i>
                  <span className="textStar" style={{ color: "#f6ba35" }}>
                    4.9
                  </span>
                  <span
                    className="colorCardTitle"
                    style={{ fontSize: "12px", color: "#8c8c8c" }}
                  >
                    (7840)
                  </span>
                </div>
              </div>
            </NavLink>
          </div>
        );
      });
    }
  };

  const renderkhThamKhao = () => {
    if (courseList !== null) {
      return courseList.slice(8, 12).map((item) => {
        return (
          <div key={item.maKhoaHoc} className="cardGlobalRes pb-3">
            <div className="cardGlobal">
              <img
                style={{
                  height: "185px",
                  maxHeight: "185px",
                }}
                src={item.hinhAnh}
                alt={item.biDanh}
              />
              <span className="stickerCard">
                {item.danhMucKhoaHoc.tenDanhMucKhoaHoc}
              </span>
              <div className="cardBodyGolbal">
                <p
                  className="text-3xl"
                  style={{
                    padding: "0 0 10px",
                    fontWeight: "500",
                    fontSize: "16px",
                  }}
                >
                  {item.moTa.substring(0, 40) + "..."}
                </p>
                <div className="cardIcon">
                  <span>
                    <i className="far fa-clock iconOclock"></i>8 giờ
                  </span>
                  <span>
                    <i className="far fa-calendar-alt iconCalendar"></i>4 tuần
                  </span>
                  <span>
                    <i className="fas fa-signal iconLevel"></i>
                    Tất cả
                  </span>
                </div>
              </div>
              <div className="cardFooter">
                <div className="titleMaker flex items-center">
                  <div className="imgCardFooter">
                    <img
                      src="https://demo2.cybersoft.edu.vn/static/media/avatar2.bb9626e2.png"
                      alt="hinhAnh"
                    />
                  </div>
                  <span className="ml-2 colorCardTitle">Elon Musk</span>
                </div>
                <div className="">
                  <p style={{ fontSize: "12px", color: "#8c8c8c" }}>
                    800,000
                    <sub>đ</sub>
                  </p>{" "}
                  <p>
                    400,000
                    <sub>đ</sub>
                    <i className="fas fa-tag iconTag"></i>
                  </p>
                </div>
              </div>
              <div className="subCard">
                <div className="subCardHead flex items-center space-x-3">
                  <img
                    src="https://demo2.cybersoft.edu.vn/static/media/emoji.6d1b7051.png"
                    alt="hinhAnh"
                  />
                  <span className="ml-1 colorCardTitle">Luong Tran</span>
                </div>
                <p style={{ fontSize: "1rem", fontWeight: "500" }}>
                  BOOTCAMP - LẬP TRÌNH FULL STACK TỪ ZERO ĐẾN CÓ VIỆC{" "}
                </p>
                <p className="colorCardTitle" style={{ padding: "10px 0" }}>
                  Đã có hơn 6200 bạn đăng kí học và có việc làm thông qua chương
                  trình đào tạo Bootcamp Lập trình Front End chuyên nghiệp. Khóa
                  học 100% thực hành cường độ cao theo dự án thực tế và kết nối
                  doanh nghiệp hỗ trợ tìm việc ngay sau khi học...
                </p>
                <div className="cardIcon">
                  <span>
                    <i className="far fa-clock iconOclock"></i>8 giờ
                  </span>
                  <span>
                    <i className="far fa-calendar-alt iconCalendar"></i>4 tuần
                  </span>
                  <span>
                    <i className="fas fa-signal iconLevel"></i>
                    Tất cả
                  </span>
                </div>

                <NavLink
                  to={`/detail/${item.maKhoaHoc}`}
                  className="text-white "
                >
                  <button className="btnGolbal w-full btnSubCard">
                    Xem chi tiết
                  </button>
                </NavLink>
              </div>
              <div className="cardSale">
                <span>Yêu thích</span>
              </div>
            </div>
          </div>
        );
      });
    }
  };

  const renderkhFE = () => {
    if (courseList !== null) {
      return courseList.slice(14, 18).map((item) => {
        return (
          <div key={item.maKhoaHoc} className="cardGlobalRes pb-3">
            <div className="cardGlobal">
              <img
                style={{
                  height: "185px",
                  maxHeight: "185px",
                }}
                src={item.hinhAnh}
                alt={item.biDanh}
              />
              <span className="stickerCard">
                {item.danhMucKhoaHoc.tenDanhMucKhoaHoc}
              </span>
              <div className="cardBodyGolbal">
                <p
                  className="text-3xl"
                  style={{
                    padding: "0 0 10px",
                    fontWeight: "500",
                    fontSize: "16px",
                  }}
                >
                  {item.moTa.substring(0, 40) + "..."}
                </p>
                <div className="cardIcon">
                  <span>
                    <i className="far fa-clock iconOclock"></i>8 giờ
                  </span>
                  <span>
                    <i className="far fa-calendar-alt iconCalendar"></i>4 tuần
                  </span>
                  <span>
                    <i className="fas fa-signal iconLevel"></i>
                    Tất cả
                  </span>
                </div>
              </div>
              <div className="cardFooter">
                <div className="titleMaker flex items-center">
                  <div className="imgCardFooter">
                    <img
                      src="https://demo2.cybersoft.edu.vn/static/media/avatar2.bb9626e2.png"
                      alt="hinhAnh"
                    />
                  </div>
                  <span className="ml-2 colorCardTitle">Elon Musk</span>
                </div>
                <div className="">
                  <p style={{ fontSize: "12px", color: "#8c8c8c" }}>
                    800,000
                    <sub>đ</sub>
                  </p>{" "}
                  <p>
                    400,000
                    <sub>đ</sub>
                    <i className="fas fa-tag iconTag"></i>
                  </p>
                </div>
              </div>
              <div className="subCard">
                <div className="subCardHead flex items-center space-x-3">
                  <img
                    src="https://demo2.cybersoft.edu.vn/static/media/emoji.6d1b7051.png"
                    alt="hinhAnh"
                  />
                  <span className="ml-1 colorCardTitle">Luong Tran</span>
                </div>
                <p style={{ fontSize: "1rem", fontWeight: "500" }}>
                  BOOTCAMP - LẬP TRÌNH FULL STACK TỪ ZERO ĐẾN CÓ VIỆC{" "}
                </p>
                <p className="colorCardTitle" style={{ padding: "10px 0" }}>
                  Đã có hơn 6200 bạn đăng kí học và có việc làm thông qua chương
                  trình đào tạo Bootcamp Lập trình Front End chuyên nghiệp. Khóa
                  học 100% thực hành cường độ cao theo dự án thực tế và kết nối
                  doanh nghiệp hỗ trợ tìm việc ngay sau khi học...
                </p>
                <div className="cardIcon">
                  <span>
                    <i className="far fa-clock iconOclock"></i>8 giờ
                  </span>
                  <span>
                    <i className="far fa-calendar-alt iconCalendar"></i>4 tuần
                  </span>
                  <span>
                    <i className="fas fa-signal iconLevel"></i>
                    Tất cả
                  </span>
                </div>
                <button className="btnGolbal btnSubCard">
                  <NavLink
                    to={`/detail/${item.maKhoaHoc}`}
                    className="text-white"
                  >
                    Xem Chi Tiết
                  </NavLink>
                </button>
              </div>
              <div className="cardSale">
                <span>Yêu thích</span>
              </div>
            </div>
          </div>
        );
      });
    }
  };

  return (
    <div style={{ padding: "0 50px" }}>
      <p style={{ color: "#f6ba35", fontWeight: "bold", padding: "20px 0" }}>
        Khóa học phổ biến
      </p>
      <div className="courseCard grid grid-cols-2">{renderKhPhoBien()}</div>
      <div className="mt-5 ">
        <p style={{ color: "#000", fontWeight: "bold", padding: "20px 0" }}>
          Khóa học tham khảo
        </p>
        <div className="courseCard grid grid-cols-2">{renderkhThamKhao()}</div>

        <div className="mt-5">
          <p style={{ color: "#000", fontWeight: "bold", padding: "20px 0" }}>
            Khóa học Font End React Js
          </p>
          <div className="grid grid-cols-2">{renderkhFE()} </div>
        </div>
      </div>
    </div>
  );
}
