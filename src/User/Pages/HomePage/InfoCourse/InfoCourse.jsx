import React from "react";

import { Desktop, Mobile, Tablet } from "../../../../responsive/responsive";
import InfoDesktop from "./InfoDesktop";
import InfoTablet from "./InfoTablet";
import InfoMoblie from "./InfoMobile";
export default function InfoCourse() {
  return (
    <>
      <Desktop>
        <InfoDesktop />
      </Desktop>

      <Tablet>
        <InfoTablet />
      </Tablet>

      <Mobile>
        <InfoMoblie />
      </Mobile>
    </>
  );
}
