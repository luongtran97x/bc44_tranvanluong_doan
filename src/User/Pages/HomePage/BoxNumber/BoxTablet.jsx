import React from 'react'
import CountUp from 'react-countup';
export default function BoxTablet() {
  return (
    <div style={{padding:"50px",marginTop:"3rem",background:"#f0f8ff"}}>
        <div className="grid grid-cols-2 ">
            <div className="flex items-center justify-center py-5">
                <div className="boxNumber text-center ">
                    <div className="img flex justify-center">
                        <img style={{height:"80px",width:"80px"}} src="https://demo2.cybersoft.edu.vn/static/media/003-students.e1a7c67b.png" alt="img" />
                    </div>
                    <div className="textNumber">
                    <CountUp style={{color:"#41b294",fontSize:"50px",fontWeight:"bold"}} enableScrollSpy  end={9000} />
                        <p className='font-medium'>Học Viên</p>
                    </div>                
                </div>
            </div>
            <div className="flex items-center justify-center">
                <div className="boxNumber text-center ">
                    <div className="img flex justify-center">
                        <img style={{height:"80px",width:"80px"}} src="https://demo2.cybersoft.edu.vn/static/media/001-timetable.0e009173.png" alt="img" />
                    </div>
                    <div className="textNumber">
                    <CountUp style={{color:"#41b294",fontSize:"50px",fontWeight:"bold"}} enableScrollSpy  end={1000} />
                        <p className='font-medium'>Khóa Học</p>
                    </div>                
                </div>
            </div>
            <div className="flex items-center justify-center">
                <div className="boxNumber text-center ">
                    <div className="img flex justify-center">
                        <img style={{height:"80px",width:"80px"}} src="https://demo2.cybersoft.edu.vn/static/media/002-hourglass.548810be.png" alt="img" />
                    </div>
                    <div className="textNumber">
                    <CountUp style={{color:"#41b294",fontSize:"50px",fontWeight:"bold"}} enableScrollSpy  end={33220} />
                        <p className='font-medium'>Giờ Học</p>
                    </div>                
                </div>
            </div>
            <div className="flex items-center justify-center">
                <div className="boxNumber text-center ">
                    <div className="img flex justify-center">
                        <img style={{height:"80px",width:"80px"}} src="https://demo2.cybersoft.edu.vn/static/media/004-teacher.5bbd6eec.png" alt="img" />
                    </div>
                    <div className="textNumber">
                    <CountUp style={{color:"#41b294",fontSize:"50px",fontWeight:"bold"}} enableScrollSpy   end={400} />
                        <p className='font-medium'>Giảng Viên</p>
                    </div>                
                </div>
            </div>
        </div>

    </div>
  )
}
