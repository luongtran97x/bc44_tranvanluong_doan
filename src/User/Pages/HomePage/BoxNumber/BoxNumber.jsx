import React from 'react'
import {Desktop,Tablet,Mobile} from "../../../../responsive/responsive.js"
import BoxDesktop from './BoxDesktop';
import BoxTablet from './BoxTablet';
import BoxMobile from './BoxMobile';
export default function BoxNumber() {
  return (
    <>

    <Desktop>
        <BoxDesktop/>
    </Desktop>

    <Tablet>
        <BoxTablet/>
    </Tablet>

    <Mobile>
        <BoxMobile/>
    </Mobile>
    </>
  
  )
}
