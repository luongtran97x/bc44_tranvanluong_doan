import React from "react";
import "./Home.css";

import { Desktop, Mobile, Tablet } from "../../../responsive/responsive";
import HomeDesktop from "./HomeDesktop";
import HomeTablet from "./HomeTablet";
import HomeMobile from "./HomeMobile";
export default function HomePage() {
  return (
    <>
      <Desktop>
        <HomeDesktop />
      </Desktop>

      <Tablet>
        <HomeTablet />
      </Tablet>

      <Mobile>
        <HomeMobile />
      </Mobile>
    </>
  );
}
