import React from "react";
import "./Intro.css";
export default function IntroTablet() {
  return (
    <div
      style={{
        padding: "0 50px 50px 50px",
      }}
    >
      <div className="">
        <div
          className="largeContent my-2"
          style={{
            background: "#41b294",
            backgroundImage:
              "url(https://demo2.cybersoft.edu.vn/static/media/astronaut.3c90d598.png)",
            backgroundSize: "50%",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "100% 100%",
            padding: "20px",
          }}
        >
          <div className="infoContent text-white ">
            <p className="text-3xl ">KHÓA HỌC</p>
            <p>
              <span className="font-bold">Học qua dự án thực tế</span>, học đi
              đôi với hành, không lý thuyết lan man, phân tích cội nguồn của vấn
              đề, xây dựng từ các ví dụ nhỏ đến thực thi một dự án lớn ngoài
              thực tế để học viên học xong làm được ngay
            </p>
            <ul>
              <li className="p-1">
                <i className="fas fa-check pr-3 font-bold"></i>
                <span>Hơn 1000 bài tập và dự án thực tế</span>
              </li>
              <li className="p-1">
                <i className="fas fa-check pr-3 font-bold"></i>
                <span>Công nghệ cập nhật mới nhất</span>
              </li>
              <li className="p-1">
                <i className="fas fa-check pr-3 font-bold"></i>
                <span>Hình ảnh, ví dụ, bài giảng sinh động trực quan</span>
              </li>
              <li className="p-1">
                <i className="fas fa-check pr-3 font-bold"></i>
                <span>Tư duy phân tích, giải quyết vấn đề trong dự án</span>
              </li>
              <li className="p-1">
                <i className="fas fa-check pr-3 font-bold"></i>
                <span>
                  Học tập kinh nghiệm, qui trình làm dự án, các qui chuẩn trong
                  dự án
                </span>
              </li>
              <li className="p-1">
                <i className="fas fa-check pr-3 font-bold"></i>
                <span>
                  Cơ hội thực tập tại các công ty lớn như FPT, Microsoft
                </span>
              </li>
            </ul>
          </div>
        </div>
        <div
          className="largeContent   my-2 "
          style={{ background: "#f6ba35", padding: "20px" }}
        >
          <div className="text-white infoContent">
            <p className="text-3xl">LỘ TRÌNH PHÙ HỢP</p>
            <ul>
              <li className="p-1">
                <i className="fas fa-check pr-3 font-bold"></i>
                <span>
                  Lộ trình bài bản từ zero tới chuyên nghiệp, nâng cao
                </span>
              </li>
              <li className="p-1">
                <i className="fas fa-check pr-3 font-bold"></i>
                <span>Học, luyện tập code, kỹ thuật phân tích, soft skill</span>
              </li>
              <li className="p-1">
                <i className="fas fa-check pr-3 font-bold"></i>
                <span>
                  Huấn luyện để phát triển năng lực và niềm đam mê lập trình
                </span>
              </li>
            </ul>
          </div>
        </div>
        <div
          className="largeContent my-2 "
          style={{ background: "#5c8295", padding: "20px" }}
        >
          <div className="text-white infoContent">
            <p className="text-3xl">HỆ THỐNG HỌC TẬP</p>
            <ul>
              <li className="p-1">
                <i className="fas fa-check  pr-3 font-bold"></i>
                <span>
                  Tự động chấm điểm trắc nghiệm và đưa câu hỏi tùy theo mức độ
                  học viên
                </span>
              </li>
              <li className="p-1">
                <i className="fas fa-check pr-3 font-bold"></i>
                <span>
                  Thống kê lượt xem video, làm bài, điểm số theo chu kỳ
                </span>
              </li>{" "}
              <li className="p-1">
                <i className="fas fa-check pr-3 font-bold"></i>
                <span>
                  Thống kê, so sánh khả năng học của các học viên cùng level để
                  đưa ra mục tiêu học tập
                </span>
              </li>
            </ul>
          </div>
        </div>
        <div
          className="largeContent my-2 "
          style={{ background: "#f6ba35", padding: "20px" }}
        >
          <div className="text-white infoContent">
            <p className="text-3xl">GIẢNG VIÊN</p>
            <ul>
              <li>
                <i className="fas fa-check pr-3 font-bold"></i>
                <span>
                  Tương tác cùng mentor và giảng viên qua phần thảo luận
                </span>
              </li>
              <li>
                <i className="fas fa-check pr-3 font-bold"></i>
                <span>Review code và đưa ra các nhận xét góp ý</span>
              </li>{" "}
              <li>
                <i className="fas fa-check pr-3 font-bold"></i>
                <span>Chấm điểm tương tác thảo luận giữa các học viên</span>
              </li>
            </ul>
          </div>
        </div>
        <div
          className="largeContent  "
          style={{ background: "#63c0a8", padding: "20px" }}
        >
          <div className="text-white infoContent">
            <p className="text-3xl">CHỨNG NHẬN</p>
            <ul>
              <li className="p-1">
                <i className="fas fa-check pr-3 font-bold"></i>
                <span>Chấm bài và có thể vấn đáp trực tuyến để review</span>
              </li>
              <li className="p-1">
                <i className="fas fa-check pr-3 font-bold"></i>
                <span>
                  Hệ thống của chúng tôi cũng tạo ra cho bạn một CV trực tuyến
                  độc đáo
                </span>
              </li>{" "}
              <li className="p-1">
                <i className="fas fa-check pr-3 font-bold"></i>
                <span>
                  Kết nối CV của bạn đến với các đối tác của V learning
                </span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}
