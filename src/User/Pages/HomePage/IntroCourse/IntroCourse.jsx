import React from "react";

import { Desktop, Mobile, Tablet } from "../../../../responsive/responsive";
import IntroDesktop from "./IntroDesktop";
import IntroTablet from "./IntroTablet";
import IntroMobile from "./IntroMobile";
export default function IntroCourse() {
  return (
    <>
      <Desktop>
        <IntroDesktop />
      </Desktop>

      <Tablet>
        <IntroTablet />
      </Tablet>

      <Mobile>
        <IntroMobile />
      </Mobile>
    </>
  );
}
