import React from "react";
import "./Student.css";
export default function StudentDesktop() {
  return (
    <div className="pb-3">
        <div
          className="review mt-10 relative"
          style={{
            padding: "25px 50px",
          }}
        >
          <div className="reviewStudent">
            <div className="triangleTopRightR "></div>
            <div className="smallBoxR smallBoxLeftTopR"></div>
            <div className="smallBoxR smallBoxRightBottomR"></div>
            <div className="smallBoxR smallBoxRightTopR"></div>
            <div className="smallBoxR smallBoxLeftBottomR"></div>
            <div className="grid grid-cols-2">
              <div className="reviewImg">
                <div className="bgStudentReview">
                  <img
                    src="https://demo2.cybersoft.edu.vn/static/media/avatarReview.2f5a1f3c.png"
                    alt="img"
                  />
                </div>
              </div>
              <div className="quoteRight">
                <blockquote className="textQoute">
                  <q>
                    Chương trình giảng dạy được biên soạn dành riêng cho các bạn Lập
                    trình từ trái ngành hoặc đã có kiến thức theo cường độ cao, luôn
                    được tinh chỉnh và tối ưu hóa theo thời gian bởi các thành viên
                    sáng lập và giảng viên dày kinh nghiệm.Thực sự rất hay và hấp
                    dẫn
                  </q>
                </blockquote>
                <p className="mt-2" style={{color:"#ed85ab"}}>Tlinh Dev</p>
                <span style={{color:"#8c8c8c"}}>Học viên xuất sắc</span>
              </div>
            </div>
          </div>
        </div>
    </div>
  );
}
