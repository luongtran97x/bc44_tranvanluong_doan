import React from "react";
import "./Student.css";
import { Desktop, Mobile, Tablet } from "../../../../responsive/responsive";
import StudentDesktop from "./StudenDesktop";
import StudentTablet from "./StudentTablet";
import StudentMobile from "./StudentMobile";
export default function Student() {
  return (
    <>
          <Desktop> 
              <StudentDesktop/>
          </Desktop>

            <Tablet>
              <StudentTablet/>
            </Tablet>

            <Mobile>
              <StudentMobile/>
            </Mobile>
    </>
  );
}
