import React from "react";
import "./Search.css";
import { Desktop, Mobile, Tablet } from "../../../responsive/responsive";
import SearchDesktop from "./SearchDesktop";
import SearchTablet from "./SearchTablet";
import SearchMobile from "./SearchMobile";
export default function Search() {


  return (
    <>
              <Desktop>
                <SearchDesktop/>
              </Desktop>

              <Tablet>
                <SearchTablet/>
              </Tablet>

              <Mobile>
                <SearchMobile/>
              </Mobile>

    </>
  );
}
