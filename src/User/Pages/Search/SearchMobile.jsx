import React from "react";
import "./Search.css";
import { NavLink, useParams } from "react-router-dom";
import { useEffect } from "react";
import { https } from "../../../services/config";
import { useState } from "react";
export default function SearchMobile() {
  const { key } = useParams();
  const [searchCourse, setSearchCourse] = useState();

  useEffect(() => {
    https
      .get(`api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?tenKhoaHoc=${key}&MaNhom=GP01`)
      .then((res) => {
        setSearchCourse(res.data);
      })
      .catch((err) => {
      });
  }, [key]);

  const renderSearchCourse = () => {
    return (
      searchCourse &&
      searchCourse.map((item, index) => {
        return (
          <div key={index} className="py-5">
            <div className="col-span-2">
              <img className="imgNet" src={item.hinhAnh} alt={item.biDanh} />
            </div>
            <div className="col-span-3 cardContent">
              <h6>{item.tenKhoaHoc}</h6>
              <p className="colorCardTitle">{item.moTa}</p>
              <div className="iconNetCard">
                <span className="textCardTitle">
                  <i className="fas fa-clock iconOclock"></i>8 giờ
                </span>
                <span className="textCardTitle">
                  <i className="fas fa-calendar iconCalendar"></i>
                  23 giờ
                </span>
                <span className="textCardTitle">
                  <i className="fas fa-signal iconLevel"></i>
                  All lever
                </span>
              </div>
              <p className="iconStarNet">
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
              </p>
            </div>
            <div className="col-span-1">
              <button className="btnGolbal">
                <NavLink to={`/detail/${item.maKhoaHoc}`}>XEM CHI TIẾT</NavLink>
              </button>
            </div>
          </div>
        );
      })
    );
  };
  const renderSearchCourseEmpty = () => {
    return (
      <div className="col-span-7">
        <p className=" py-5 text-2xl">Không tìm thấy khóa học!</p>
      </div>
    );
  };

  return (
    <div>
      <div
        className="banner"
        style={{ padding: "50px", background: "#ffd60a", color: "white" }}
      >
        <p className="text-3xl font-medium">TÌM KIẾM </p>
        <p> KẾT QUẢ TÌM KIẾM KHÓA HỌC!!!</p>
      </div>
      <div className="searchPgae p-12">
        <div className="grid xl:grid-cols-8 md:grid-cols-8 sm:grid-cols-none">
          <div className="xl:col-span-1 md:col-span-1 hidden" >
            <div className="navFilter ">
              <h6>Lọc</h6>
              <div className="filterContainer">
                <div className="filterItem">
                  <h6>
                    Khóa học
                  </h6>
                  <ul>
                    <li>
                      <label className="BoxSearch">
                        Tất cả
                        <input type="checkbox" name="" id="" />
                        <span className="checkMark">
                          <i className="fas fa-check"></i>
                        </span>
                      </label>
                    </li>
                    <li>
                      <label className="BoxSearch">
                        Front End
                        <input type="checkbox" name="" id="" />
                        <span className="checkMark">
                          <i className="fas fa-check"></i>
                        </span>
                      </label>
                    </li>
                    <li>
                      <label className="BoxSearch">
                        Back End
                        <input type="checkbox" name="" id="" />
                        <span className="checkMark">
                          <i className="fas fa-check"></i>
                        </span>
                      </label>
                    </li>
                    <li>
                      <label className="BoxSearch">
                        HTML / CSS
                        <input type="checkbox" name="" id="" />
                        <span className="checkMark">
                          <i className="fas fa-check"></i>
                        </span>
                      </label>
                    </li>
                  </ul>
                </div>
                <div className="filterItem">
                  <h6>
                    {" "}
                    CẤP ĐỘ
                  
                  </h6>
                  <ul>
                    <li>
                      <label className="BoxSearch">
                        Tất cả
                        <input type="checkbox" name="" id="" />
                        <span className="checkMark">
                          <i className="fas fa-check"></i>
                        </span>
                      </label>
                    </li>
                    <li>
                      <label className="BoxSearch">
                        Mới bắt đầu
                        <input type="checkbox" name="" id="" />
                        <span className="checkMark">
                          <i className="fas fa-check"></i>
                        </span>
                      </label>
                    </li>
                    <li>
                      <label className="BoxSearch">
                        Trung Cấp
                        <input type="checkbox" name="" id="" />
                        <span className="checkMark">
                          <i className="fas fa-check"></i>
                        </span>
                      </label>
                    </li>
                    <li>
                      <label className="BoxSearch">
                        Cao Cấp
                        <input type="checkbox" name="" id="" />
                        <span className="checkMark">
                          <i className="fas fa-check"></i>
                        </span>
                      </label>
                    </li>
                  </ul>
                </div>
                <div className="filterItem">
                  <h6>
                    {" "}
                    ĐÁNH GIÁ
                  
                  </h6>
                  <ul>
                    <li>
                      <label className="BoxSearch">
                        <i className="fas fa-star"></i>
                        <input type="checkbox" name="" id="" />
                        <span className="checkMark">
                          <i className="fas fa-check"></i>
                        </span>
                      </label>
                    </li>
                    <li>
                      <label className="BoxSearch">
                        <i className="fas fa-star"></i>{" "}
                        <i className="fas fa-star"></i>
                        <input type="checkbox" name="" id="" />
                        <span className="checkMark">
                          <i className="fas fa-check"></i>
                        </span>
                      </label>
                    </li>
                    <li>
                      <label className="BoxSearch">
                        <i className="fas fa-star"></i>{" "}
                        <i className="fas fa-star"></i>{" "}
                        <i className="fas fa-star"></i>
                        <input type="checkbox" name="" id="" />
                        <span className="checkMark">
                          <i className="fas fa-check"></i>
                        </span>
                      </label>
                    </li>
                    <li>
                      <label className="BoxSearch">
                        <i className="fas fa-star"></i>{" "}
                        <i className="fas fa-star"></i>{" "}
                        <i className="fas fa-star"></i>{" "}
                        <i className="fas fa-star"></i>
                        <input type="checkbox" name="" id="" />
                        <span className="checkMark">
                          <i className="fas fa-check"></i>
                        </span>
                      </label>
                    </li>
                    <li>
                      <label className="BoxSearch">
                        <i className="fas fa-star"></i>{" "}
                        <i className="fas fa-star"></i>{" "}
                        <i className="fas fa-star"></i>{" "}
                        <i className="fas fa-star"></i>
                        <i className="fas fa-star"></i>
                        <input type="checkbox" name="" id="" />
                        <span className="checkMark">
                          <i className="fas fa-check"></i>
                        </span>
                      </label>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div className="col-span-7">
            <p className="text-xl py-2">
              Hiển thị {searchCourse && searchCourse.length} kết quả
            </p>
            {searchCourse === undefined
              ? renderSearchCourseEmpty()
              : renderSearchCourse()}
          </div>
        </div>
      </div>
    </div>
  );
}
