import React from "react";
import "./NotFound.css"
export default function NotFoundPage() {
  return (
    <section class="page_404">
   
        <div class="row">
          <div class="col-sm-12 ">
            <div class="col-sm-10 col-sm-offset-1  text-center">
              <div class="four_zero_four_bg">
                <h1 class="text-center ">404</h1>
              </div>

              <div class="contant_box_404">
                <h3 class="h2">Đường dẫn không chính xác</h3>


                <a href="/" class="link_404">
                  Quay trở về trang chủ
                </a>
              </div>
            </div>
          </div>
        </div>
    
    </section>
  );
}
