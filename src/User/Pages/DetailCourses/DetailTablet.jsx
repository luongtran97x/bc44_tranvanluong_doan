import React, { useEffect, useState } from "react";
import "./Detail.css";
import { NavLink, useParams } from "react-router-dom";
import { https } from "../../../services/config";
import { useSelector } from "react-redux";
import { LocalService } from "../../../services/LocalService";
import ReactModal from "react-modal";
import { useSpring, animated } from "react-spring";
import {
  CheckOutlined,
  CloseCircleOutlined,
} from "@ant-design/icons";
export default function DetailTablet() {
  const [relatedCourses, setRealtedCourses] = useState([]);
  const { id } = useParams();
  const [courses, setCourses] = useState({});
  const user = useSelector((state) => state.userSlice.userInfo);
  const userInfo = LocalService.getItem("USER_INFO")
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [modalIsOpen2, setModalIsOpen2] = useState(false);
  const [modalIsOpen3, setModalIsOpen3] = useState(false);
  useEffect(() => {
    https
      .get(`api/QuanLyKhoaHoc/LayThongTinKhoaHoc?maKhoaHoc=${id}`)
      .then((res) => {
        setCourses(res.data);
        fetchListRealted(res.data.danhMucKhoaHoc.maDanhMucKhoahoc);
      })
      .catch((err) => {});
  }, []);
  const customStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%,-50%) ",
      width: "auto",
      height: "auto",
      borderRadius: "30px",
      background: "#020d18",
    },
  };
  const props = useSpring({
    opacity: modalIsOpen ? 1 : 0,
    transform: modalIsOpen ? "translateY(0%)" : "translateY(-100%)",
  });
  const props2 = useSpring({
    opacity: modalIsOpen2 ? 1 : 0,
    transform: modalIsOpen2 ? "translateY(0%)" : "translateY(-100%)",
  });
  const props3 = useSpring({
    opacity: modalIsOpen3 ? 1 : 0,
    transform: modalIsOpen3 ? "translateY(0%)" : "translateY(-100%)",
  });

  const openModal = () => {
    setModalIsOpen(true);
  };
  const closeModal = () => {
    setModalIsOpen(false);
  };

  const openModal2 = () => {
    setModalIsOpen2(true);
  };
  const closeModal2 = () => {
    setModalIsOpen2(false);
  };
  const openModal3 = () => {
    setModalIsOpen3(true);
  };
  const closeModal3 = () => {
    setModalIsOpen3(false);
  };

  //  xử lý đăng ký khóa học
  const handelOnCLick = () => {
    if (!userInfo) {
      openModal()
      return
    }

    let data = {
      maKhoaHoc: id,
      taiKhoan: userInfo.taiKhoan,
    };

    let local = LocalService.getItem("USER_INFO");
    let config = {
      headers: {
        Authorization: `Bearer ${local.accessToken}`,
      },
    };
    https
      .post("api/QuanLyKhoaHoc/DangKyKhoaHoc", data, config)
      .then((res) => {
        openModal2()
      })
      .catch((err) => {
        openModal3()
      });
  };

  const fetchListRealted = async (danhMuc) => {
    try {
      if (danhMuc.length !== 0) {
        const res = await https.get(
          `api/QuanLyKhoaHoc/LayKhoaHocTheoDanhMuc?maDanhMuc=${danhMuc}&MaNhom=GP01`
        );
        setRealtedCourses(res.data);
      }
    } catch (err) {
      console.log("🚀 ~ err:", err);
    }
  };

  const renderListRealed = () => {
    return (
      relatedCourses &&
      relatedCourses.slice(0, 4).map((item) => {
        return (
          <div key={item.maKhoaHoc} className="cardGlobalRes md:py-2">
            <div className="cardGlobal">
              <img
                style={{
                  height: "185px",
                  maxHeight: "185px",
                }}
                src={item.hinhAnh}
                alt={item.biDanh}
              />
              <span className="stickerCard">
                {item.danhMucKhoaHoc.tenDanhMucKhoaHoc}
              </span>
              <div className="cardBodyGolbal">
                <p
                  className="text-3xl"
                  style={{
                    padding: "0 0 10px",
                    fontWeight: "500",
                    fontSize: "16px",
                  }}
                >
                  {item.moTa.slice(0, 50) + "..."}
                </p>
                <div className="cardIcon">
                  <span>
                    <i className="far fa-clock iconOclock"></i>8 giờ
                  </span>
                  <span>
                    <i className="far fa-calendar-alt iconCalendar"></i>4 tuần
                  </span>
                  <span>
                    <i className="fas fa-signal iconLevel"></i>
                    Tất cả
                  </span>
                </div>
              </div>
              <div className="cardFooter">
                <div className="titleMaker flex items-center">
                  <div className="imgCardFooter">
                    <img
                      src="https://demo2.cybersoft.edu.vn/static/media/avatar2.bb9626e2.png"
                      alt="hinhAnh"
                    />
                  </div>
                  <span className="ml-2 colorCardTitle">Elon Musk</span>
                </div>
                <div className="">
                  <p style={{ fontSize: "12px", color: "#8c8c8c" }}>
                    800,000
                    <sub>đ</sub>
                  </p>{" "}
                  <p>
                    400,000
                    <sub>đ</sub>
                    <i className="fas fa-tag iconTag"></i>
                  </p>
                </div>
              </div>
              <div className="subCard">
                <div className="subCardHead flex items-center space-x-3">
                  <img
                    src="https://demo2.cybersoft.edu.vn/static/media/emoji.6d1b7051.png"
                    alt="hinhAnh"
                  />
                  <span className="ml-1 colorCardTitle">Luong Tran</span>
                </div>
                <p style={{ fontSize: "1rem", fontWeight: "500" }}>
                  BOOTCAMP - LẬP TRÌNH FULL STACK TỪ ZERO ĐẾN CÓ VIỆC{" "}
                </p>
                <p className="colorCardTitle" style={{ padding: "10px 0" }}>
                  Đã có hơn 6200 bạn đăng kí học và có việc làm thông qua chương
                  trình đào tạo Bootcamp Lập trình Front End chuyên nghiệp. Khóa
                  học 100% thực hành cường độ cao theo dự án thực tế và kết nối
                  doanh nghiệp hỗ trợ tìm việc ngay sau khi học...
                </p>
                <div className="cardIcon">
                  <span>
                    <i className="far fa-clock iconOclock"></i>8 giờ
                  </span>
                  <span>
                    <i className="far fa-calendar-alt iconCalendar"></i>4 tuần
                  </span>
                  <span>
                    <i className="fas fa-signal iconLevel"></i>
                    Tất cả
                  </span>
                </div>

                <NavLink
                  to={`/detail/${item.maKhoaHoc}`}
                  className="text-white "
                >
                  <button className="btnGolbal w-full btnSubCard">
                    Xem chi tiết
                  </button>
                </NavLink>
              </div>
              <div className="cardSale">
                <span>Yêu thích</span>
              </div>
            </div>
          </div>
        );
      })
    );
  };

  return (
    <>
    {/* modal thông báo đăng ký thất bại */}
     <ReactModal
        onRequestClose={closeModal3}
        isOpen={modalIsOpen3}
        style={customStyles}
      >
        <animated.div style={props3}>
          <div className="text-center">
            <div className="text-orange-600 rounded-full text-5xl">
              <CloseCircleOutlined />
            </div>
            <p className="font-bold text-2xl pt-2 text-white">
              Bạn đã đăng khóa học này rồi!
            </p>
          </div>
        </animated.div>
      </ReactModal>
      {/* modal kiểm tra đăng nhập */}
      <ReactModal
        onRequestClose={closeModal}
        isOpen={modalIsOpen}
        style={customStyles}
      >
        <animated.div style={props}>
          <div className="text-center">
            <div className="text-orange-600 rounded-full text-5xl">
              <CloseCircleOutlined />
            </div>
            <p className="font-bold text-2xl pt-2 text-white">
              Vui Lòng Đăng Nhập Trước Khi Đăng Ký
            </p>
          </div>
        </animated.div>
      </ReactModal>
      {/* Thông báo đăng ký thành công */}
      <ReactModal
        onRequestClose={closeModal2}
        isOpen={modalIsOpen2}
        style={customStyles}
      >
        <animated.div style={props2}>
          <div className="text-orange-600 text-center rounded-full text-5xl">
            <CheckOutlined />
          </div>
          <p className="font-bold text-2xl pt-2 text-white">
            Đăng ký thành công
          </p>
        </animated.div>
      </ReactModal>
      <div className="titleCourse">
        <h3>Thông tin khóa học</h3>
        <p>Tiến lên và không chần chừ !!!</p>
      </div>
      <div className="detailCouresContent">
        <div className="grid grid-cols-6">
          <div className="col-span-4">
            <h4 className="titleDetailCourse font-medium text-2xl">
              LẬP TRÌNH FRONT-END CHUYÊN NGHIỆP
            </h4>
            <div className="grid grid-cols-3 headDetailCourse">
              <div className="">
                <div className="detailCourseIntro">
                  <div className="">
                    <img
                      src="https://demo2.cybersoft.edu.vn/static/media/instrutor5.2e4bd1e6.jpg"
                      alt="img"
                    />
                  </div>
                  <div className="instrutorTitle">
                    <p
                      style={{
                        color: "#8c8c8c",
                      }}
                    >
                      Giảng viên
                    </p>
                    <p>Robert Ngô Ngọc</p>
                  </div>
                </div>
              </div>
              <div className="">
                <div className="detailCourseIntro">
                  <div className="">
                    <i
                      style={{
                        color: "#41b294",
                      }}
                      className="fas fa-graduation-cap"
                    ></i>
                  </div>
                  <div className="instrutorTitle">
                    <p
                      style={{
                        color: "#8c8c8c",
                      }}
                    >
                      Lĩnh vực
                    </p>
                    <p>Tư duy lập trình</p>
                  </div>
                </div>
              </div>
              <div className="">
                <div className="detailCourseIntro">
                  <div className="reviewDetail">
                    <span>
                      <i
                        style={{
                          color: "#f6ba35",
                          fontSize: "20px",
                        }}
                        className="fas fa-star"
                      ></i>
                      <i
                        style={{
                          color: "#f6ba35",
                          fontSize: "20px",
                        }}
                        className="fas fa-star"
                      ></i>
                      <i
                        style={{
                          color: "#f6ba35",
                          fontSize: "20px",
                        }}
                        className="fas fa-star"
                      ></i>
                      <i
                        style={{
                          color: "#f6ba35",
                          fontSize: "20px",
                        }}
                        className="fas fa-star-half-alt"
                      ></i>
                      <i
                        style={{
                          color: "#f6ba35",
                          fontSize: "20px",
                        }}
                        className="far fa-star"
                      ></i>
                      3.5
                    </span>
                    <p style={{ color: "#8c8c8c" }}>100 đánh giá</p>
                  </div>
                </div>
              </div>
            </div>
            <p className="textDiscripts" style={{ color: "#8c8c8c" }}>
              React.js là thư viện JavaScript phổ biến nhất mà bạn có thể sử
              dụng và tìm hiểu ngày nay để xây dựng giao diện người dùng hiện
              đại, phản ứng cho web.Khóa học này dạy bạn về React chuyên sâu, từ
              cơ bản, từng bước đi sâu vào tất cả các kiến ​​thức cơ bản cốt
              lõi, khám phá rất nhiều ví dụ và cũng giới thiệu cho bạn các khái
              niệm nâng cao.Bạn sẽ nhận được tất cả lý thuyết, hàng tấn ví dụ và
              bản trình diễn, bài tập và bài tập cũng như vô số kiến ​​thức quan
              trọng bị hầu hết các nguồn khác bỏ qua - sau cùng, có một lý do
              tại sao khóa học này lại rất lớn! Và trong trường hợp bạn thậm chí
              không biết tại sao bạn lại muốn học React và bạn chỉ ở đây vì một
              số quảng cáo hoặc "thuật toán" - đừng lo lắng: ReactJS là một công
              nghệ quan trọng với tư cách là một nhà phát triển web và trong
              khóa học này, tôi sẽ cũng giải thích TẠI SAO điều đó lại quan
              trọng!
            </p>
            <div className="boxCourseLearn">
              <h6 className="font-medium">Những gì bạn sẽ học</h6>
              <div className="grid grid-cols-2 ">
                <div className="">
                  <ul>
                    <li>
                      <i className="fas fa-check"></i>
                      <a href="#">
                        Xây dựng các ứng dụng web mạnh mẽ, nhanh chóng, thân
                        thiện với người dùng và phản ứng nhanh
                      </a>
                    </li>
                    <li>
                      <i className="fas fa-check"></i>
                      <a href="#">
                        Đăng ký công việc được trả lương cao hoặc làm freelancer
                        trong một trong những lĩnh vực được yêu cầu nhiều nhất
                        mà bạn có thể tìm thấy trong web dev ngay bây giờ
                      </a>
                    </li>

                    <li>
                      <i className="fas fa-check"></i>
                      <a href="#">
                        Cung cấp trải nghiệm người dùng tuyệt vời bằng cách tận
                        dụng sức mạnh của JavaScript một cách dễ dàng
                      </a>
                    </li>
                    <li>
                      <i className="fas fa-check"></i>
                      <a href="#">
                        Tìm hiểu tất cả về React Hooks và React Components
                      </a>
                    </li>
                  </ul>
                </div>
                <div className="">
                  <ul>
                    <li>
                      <i className="fas fa-check"></i>
                      <a href="#">
                        Thông thạo chuỗi công cụ hỗ trợ React, bao gồm cú pháp
                        Javascript NPM, Webpack, Babel và ES6 / ES2015
                      </a>
                    </li>
                    <li>
                      <i className="fas fa-check"></i>
                      <a href="#">
                        Nhận ra sức mạnh của việc xây dựng các thành phần có thể
                        kết hợp
                      </a>
                    </li>

                    <li>
                      <i className="fas fa-check"></i>
                      <a href="#">
                        Hãy là kỹ sư giải thích cách hoạt động của Redux cho mọi
                        người, bởi vì bạn biết rất rõ các nguyên tắc cơ bản
                      </a>
                    </li>
                    <li>
                      <i className="fas fa-check"></i>
                      <a href="#">
                        Nắm vững các khái niệm cơ bản đằng sau việc cấu trúc các
                        ứng dụng Redux
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="courseContent">
              <h6>Nội dung khóa học</h6>
              <div className="courseDetailItem">
                <div className="courseDetailContent">
                  <div className="sectionCourse">
                    <span>Mục 1: Giới thiệu</span>
                    <button className="btnGlobal btnPreview">Xem trước</button>
                  </div>
                  <p>Bài học</p>
                  <div className="lessonContainer">
                    <div className="lessonContent mt-1">
                      <span>
                        <i className="fas fa-play-circle"></i>
                        Các khái niệm về React Component
                      </span>
                    </div>
                    <div className="lessonContent mt-1">
                      <span>
                        <i className="fas fa-play-circle"></i>
                        Thiết lập môi trường cho Windows
                      </span>
                      <span>
                        <i className="fas fa-clock"></i>
                        14:35
                      </span>
                    </div>
                    <div className="lessonContent mt-1">
                      <span>
                        <i className="fas fa-play-circle"></i>
                        Tạo ứng dụng React - React-Scripts
                      </span>
                      <span>
                        <i className="fas fa-clock"></i>
                        14:35
                      </span>
                    </div>
                    <div className="lessonContent mt-1">
                      <span>
                        <i className="fas fa-play-circle"></i>
                        Ghi chú nhanh về dấu ngoặc kép cho string interpolation
                      </span>
                      <span>
                        <i className="fas fa-clock"></i>
                        14:35
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="courseDetailItem">
                <div className="courseDetailContent">
                  <span className="font-normal text-xl mr-2">
                    Mục 2: KIẾN THỨC CĂN BẢN
                  </span>
                  <button className="btnGlobal btnPreview">Xem trước</button>
                </div>
                <p>Bài học</p>
                <div className="lessonContainer">
                  <div className="lessonContent mt-1">
                    <span>
                      <i className="fas fa-play-circle"></i>
                      Trang chủ và thành phần thư mục
                    </span>
                  </div>
                  <div className="lessonContent mt-1">
                    <span>
                      <i className="fas fa-play-circle"></i>
                      Hướng dẫn khóa học + Liên kết Github
                    </span>
                    <span>
                      <i className="fas fa-clock"></i>
                      14:35
                    </span>
                  </div>
                  <div className="lessonContent mt-1">
                    <span>
                      <i className="fas fa-play-circle"></i>
                      Trang chủ thương mại điện tử + thiết lập SASS
                    </span>
                    <span>
                      <i className="fas fa-clock"></i>
                      14:35
                    </span>
                  </div>
                  <div className="lessonContent mt-1">
                    <span>
                      <i className="fas fa-play-circle"></i>
                      Tệp CSS và SCSS
                    </span>
                    <span>
                      <i className="fas fa-clock"></i>
                      14:35
                    </span>
                  </div>
                  <div className="lessonContent mt-1">
                    <span>
                      <i className="fas fa-play-circle"></i>
                      React 17: Cập nhật các gói + Phiên bản React mới nhất
                    </span>
                    <span>
                      <i className="fas fa-clock"></i>
                      14:35
                    </span>
                  </div>
                </div>
              </div>
              <div className="courseDetailItem">
                <div className="courseDetailContent">
                  <span className="font-normal text-xl mr-2">
                    MỤC 3: KIẾN THỨC CHUYÊN SÂU
                  </span>
                  <button className="btnGlobal btnPreview">Xem trước</button>
                </div>
                <p>Bài học</p>
                <div className="lessonContainer">
                  <div className="lessonContent mt-1">
                    <span>
                      <i className="fas fa-play-circle"></i>
                      connect() and mapStateToProps
                    </span>
                  </div>
                  <div className="lessonContent mt-1">
                    <span>
                      <i className="fas fa-play-circle"></i>
                      Trạng thái thư mục vào Redux
                    </span>
                    <span>
                      <i className="fas fa-clock"></i>
                      14:35
                    </span>
                  </div>
                  <div className="lessonContent mt-1">
                    <span>
                      <i className="fas fa-play-circle"></i>
                      Thành phần Tổng quan về Bộ sưu tập
                    </span>
                    <span>
                      <i className="fas fa-clock"></i>
                      14:35
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-span-2 pl-4">
            {courses && (
              <div className="sideBarCourseDetail">
                <img src={courses.hinhAnh} alt={courses.biDanh} />
                <div className="coursePrice">
                  <p>
                    <i className="fas fa-bolt"></i>
                    500,000
                    <sup>đ</sup>
                  </p>
                </div>
                <button
                  onClick={handelOnCLick}
                  className="btnGlobal btnPreview"
                >
                  Đăng ký
                </button>
                <div className="sideBarDetailContent">
                  <ul>
                    <li>
                      <p>
                        Ghi danh
                        <span> 10 học viên</span>
                      </p>
                      <i className="fas fa-user-graduate "></i>
                    </li>
                    <li>
                      <p>
                        Thời gian:
                        <span> 18 giờ</span>
                      </p>
                      <i className="far fa-clock far fa-calendar-alt"></i>
                    </li>
                    <li>
                      <p>
                        Bài học:
                        <span>10</span>
                      </p>
                      <i className="fas fa-book"></i>
                    </li>
                    <li>
                      <p>
                        Trình độ:
                        <span> Người mới bắt đầu</span>
                      </p>
                      <i className="fas fa-database"></i>
                    </li>
                  </ul>
                  <form action="" className="formCoupon">
                    <input type="text" placeholder="Nhập mã" />
                  </form>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
      <div className="coursesRelated">
        <h6 className="pb-3">
          <a className="font-medium pt-3S " href="">
            Khóa Học Liên Quan
          </a>
        </h6>
        <div className="grid xl:grid-cols-4 md:grid-cols-2 sm:grid-cols-1 overflow-hidden">{renderListRealed()}</div>
      </div>
    </>
  );
}
