import React from "react";

import { Desktop, Mobile, Tablet } from "../../../responsive/responsive";
import DetailDesktop from "./DetailDesktop";
import DetailTablet from "./DetailTablet";
import DetailMobile from "./DetailMobile";
export default function DetailCourses() {
  return (
    <>
      <Desktop>
        <DetailDesktop />
      </Desktop>

      <Tablet>
        <DetailTablet />
      </Tablet>

      <Mobile>
        <DetailMobile />
      </Mobile>
    </>
  );
}
