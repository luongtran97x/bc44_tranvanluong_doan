import React, { useEffect } from "react";
import "./History.css";
import { https } from "../../../services/config";
import { useDispatch, useSelector } from "react-redux";
import { LocalService } from "../../../services/LocalService";
import {
  removeCourse,
  setListCourse,
} from "../../../redux/courseSlice";
import { message } from "antd";


export default function History() {
  const crudList = useSelector((state) => state.courseSlice.crudList);
  const dispatch = useDispatch();

  const user = useSelector((state) => state.userSlice.userInfo);
  let local = LocalService.getItem("USER_INFO");
  const config = {
    headers: {
      Authorization: `Bearer ${local.accessToken}`,
    },
  };
  useEffect(() => {
    https
      .post("api/QuanLyNguoiDung/ThongTinTaiKhoan", null, config)
      .then((res) => {
        dispatch(setListCourse(res.data.chiTietKhoaHocGhiDanh));
      })
      .catch((err) => {
  
      });
  }, []);
 




  const renderCourse = () => {
    return (
      crudList &&
      crudList.map((item, index) => {
        return (
          <div
            key={index}
            className="grid xl:gap-4 md:gap-4 xl:grid-cols-3  md:grid-cols-3 py-5"
          >
            <div className="">
              <img
                className="imgNet object-cover"
                width={500}
                src={item.hinhAnh}
                alt={item.biDanh}
              />
            </div>
            <div className="cardNetContent">
              <h6 className="font-bold text-xl">{item.tenKhoaHoc}</h6>
              <p className="colorCardTitle">
                ES6 là một chuẩn Javascript mới được đưa ra vào năm 2015 với
                nhiều quy tắc và cách sử dụng khác nhau...
              </p>
              <div className="iconNetCard">
                <span className="textCardTitle">
                  <i className="far fa-clock iconOclock"></i>8 giờ
                </span>
                <span className="textCardTitle">
                  <i className="far fa-calendar iconCalendar"></i>
                  23 giờ
                </span>
                <span className="textCardTitle">
                  <i className="fas fa-signal iconLevel "></i>
                  All lever
                </span>
              </div>
              <div className="iconStarNet">
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
              </div>
              <div className="flex items-center space-x-3">
                <img
                  className="imgNetFooter"
                  src="https://demo2.cybersoft.edu.vn/static/media/instrutor10.89946c43.jpg"
                  alt="img"
                />
                <span className="ml-2">Luong Tran</span>
              </div>
            </div>
            <div className="cancelNet flex justify-center items-end">
              <button
                onClick={() => {
                  let data = {
                    maKhoaHoc: item.maKhoaHoc,
                    taiKhoan: local.taiKhoan,
                  };
                  https
                    .post("api/QuanLyKhoaHoc/HuyGhiDanh", data, config)
                    .then((res) => {
                      message.success(res.data);
                      dispatch(removeCourse(item));
                    })
                    .catch((err) => {
                    });
                }}
                className="btnGolbal text-white"
              >
                HỦY KHÓA HỌC
              </button>
            </div>
          </div>
        );
      })
    );
  };

  return (
    <div>
      <div
        className="banner"
        style={{ padding: "50px", background: "#ffd60a", color: "white" }}
      >
        <p className="text-3xl font-medium">THÔNG TIN CÁ NHÂN </p>
        <p> THÔNG TIN HỌC VIÊN</p>
      </div>
      <div className="myCourse px-12">
        <div className="flex justify-between items-center">
          <p className="text-center font-bold text-2xl pt-5">
            KHÓA HỌC CỦA TÔI
          </p>
        </div>
        <div className="myCourseItem">{renderCourse()}</div>
      </div>
    </div>
  );
}
