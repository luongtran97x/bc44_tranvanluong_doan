import React, { useEffect, useState } from "react";
import { NavLink, useParams } from "react-router-dom";
import { https } from "../../../services/config";
import "./ListCourses.css";
export default function ListMobile() {
  const { id } = useParams();
  const [listCourses, setListCourses] = useState([]);
  useEffect(() => {
    https
      .get(
        `api/QuanLyKhoaHoc/LayKhoaHocTheoDanhMuc?maDanhMuc=${id}&MaNhom=GP01`
      )
      .then((res) => {
        setListCourses(res.data);
      })
      .catch((err) => {});
  }, [id]);

  const renderTenKh = () => {
    if (listCourses.length !== 0) {
      return (
        <span className="ml-2">
          {listCourses[0].danhMucKhoaHoc.tenDanhMucKhoaHoc}
        </span>
      );
    }
  };

  const renderListKh = () => {
    return listCourses.map((item) => {
      return (
        <div key={item.maKhoaHoc} className="cardGlobalRes py-5">
          <div to={`/detail/${item.maKhoaHoc}`} className="cardGlobal">
            <img
              style={{
                height: "185px",
                maxHeight: "185px",
              }}
              src={item.hinhAnh}
              alt={item.biDanh}
            />
            <span className="stickerCard">{item.tenKhoaHoc}</span>
            <div className="cardBodyGolbal">
              <p
                className="text-3xl"
                style={{
                  padding: "0 0 10px",
                  fontWeight: "500",
                  fontSize: "16px",
                }}
              >
                {item.moTa && item.moTa.substring(0, 40) + "..."}
              </p>
              <div className="cardIcon">
                <span>
                  <i className="far fa-clock iconOclock"></i>8 giờ
                </span>
                <span>
                  <i className="far fa-calendar-alt iconCalendar"></i>4 tuần
                </span>
                <span>
                  <i className="fas fa-signal iconLevel"></i>
                  Tất cả
                </span>
              </div>
            </div>
            <div className="cardFooter">
              <div className="titleMaker flex items-center">
                <div className="imgCardFooter">
                  <img
                    src="https://demo2.cybersoft.edu.vn/static/media/avatar2.bb9626e2.png"
                    alt="hinhAnh"
                  />
                </div>
                <span className="ml-2 colorCardTitle">Elon Musk</span>
              </div>
              <div className="">
                <p style={{ fontSize: "12px", color: "#8c8c8c" }}>
                  800,000
                  <sub>đ</sub>
                </p>{" "}
                <p>
                  400,000
                  <sub>đ</sub>
                  <i className="fas fa-tag iconTag"></i>
                </p>
              </div>
            </div>
            <div className="subCard">
              <div className="subCardHead flex items-center space-x-3">
                <img
                  src="https://demo2.cybersoft.edu.vn/static/media/emoji.6d1b7051.png"
                  alt="hinhAnh"
                />
                <span className="ml-1 colorCardTitle">Luong Tran</span>
              </div>
              <p style={{ fontSize: "1rem", fontWeight: "500" }}>
                BOOTCAMP - LẬP TRÌNH FULL STACK TỪ ZERO ĐẾN CÓ VIỆC{" "}
              </p>
              <p className="colorCardTitle" style={{ padding: "10px 0" }}>
                Đã có hơn 6200 bạn đăng kí học và có việc làm thông qua chương
                trình đào tạo Bootcamp Lập trình Front End chuyên nghiệp. Khóa
                học 100% thực hành cường độ cao theo dự án thực tế và kết nối
                doanh nghiệp hỗ trợ tìm việc ngay sau khi học...
              </p>
              <div className="cardIcon">
                <span>
                  <i className="far fa-clock iconOclock"></i>8 giờ
                </span>
                <span>
                  <i className="far fa-calendar-alt iconCalendar"></i>4 tuần
                </span>
                <span>
                  <i className="fas fa-signal iconLevel"></i>
                  Tất cả
                </span>
              </div>
              <button className="btnGolbal btnSubCard">
                <NavLink
                  to={`/detail/${item.maKhoaHoc}`}
                  className="text-white"
                >
                  Xem Chi Tiết
                </NavLink>
              </button>
            </div>
            <div className="cardSale">
              <span>Yêu thích</span>
            </div>
          </div>
        </div>
      );
    });
  };

  return (
    <div>
      <div
        className="banner"
        style={{ padding: "50px", background: "#ffd60a", color: "white" }}
      >
        <p className="text-lg font-medium">KHÓA HỌC THEO DANH MỤC</p>
        <p className="text-lg"> HÃY CHỌN KHÓA HỌC MONG MUỐN !!!</p>
      </div>
      <div className="listCourses" style={{ padding: "50px" }}>
        <div className="courseName">
          <div className="btn">
            <i className="fas fa-desktop"></i>
            {renderTenKh()}
          </div>
        </div>
      </div>
      <div className="mt-3">
        <div className="grid grid-cols-1 overflow-hidden">{renderListKh()}</div>
      </div>
    </div>
  );
}
