import React from "react";
import { Desktop, Mobile, Tablet } from "../../../responsive/responsive";
import ListDesktop from "./ListDesktop";
import ListTablet from "./ListTablet";
import ListMobile from "./ListMobile";
export default function ListCourses() {


  return (

    <>
    <Desktop>
      <ListDesktop/>
    </Desktop>


    <Tablet>
      <ListTablet/>
    </Tablet>


    <Mobile>
      <ListMobile/>
    </Mobile>
    </>
  );
}
