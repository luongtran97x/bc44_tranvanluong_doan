import React, { useState } from "react";
import "./Login.css";
import {
  Button,
  Col,
  Form,
  Input,
  Row,
  Select,
  Typography,
  message,
  Modal,
} from "antd";

import { https } from "../../../services/config";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setLogin } from "../../../redux/userSlice";
import { useSpring, animated } from "react-spring";
import {
  CheckSquareOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import { regex } from "../../../Constant/regex";
import { LocalService } from "../../../services/LocalService";
import { useEffect } from "react";
const onFinishFailedLogin = (errorInfo) => {};
export default function LoginDesktop() {
  // hooks
  const [err, setErr] = useState("");
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isModalOpen2, setIsModalOpen2] = useState(false);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [isActive, setIsActive] = useState(false);

  // xử lý ngăn người dùng vào trang khi đã login thành công
  useEffect(() => {
    let token = LocalService.getItem("USER_INFO");
    if (token) {
      navigate("/");
    }
  }, []);

  // xử lý đăng nhập
  const onFinishLogin = (values) => {
    https
      .post("api/QuanLyNguoiDung/DangNhap", values)
      .then((res) => {
        dispatch(setLogin(res.data));
        LocalService.setItem(res.data, "USER_INFO");
        message.success("Đăng nhập thành công!");
        setTimeout(() => {
          navigate("/");
        }, 800);
      })
      .catch((err) => {
        message.error(err.response.data);
      });
  };

  // xử lý toggle modal
  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const showModal2 = () => {
    setIsModalOpen2(true);
  };

  const handleCancel2 = () => {
    setIsModalOpen2(false);
  };

  // xử lý hiệu ứng modal
  const animation = useSpring({
    width: "100%",
    from: {
      width: "0%",
    },
    "80%": {},
    config: {
      duration: 5000,
    },
  });

  //  xử lý đăng ký
  const onFinishLogup = (values) => {
    console.log("🚀 ~ values:", values)
    https
      .post("api/QuanLyNguoiDung/DangKy", values)
      .then((res) => {
        showModal2();
      })
      .catch((err) => {
        showModal();
        setErr(err.response.data);
      });
  };
  // xử lý chuyển trang
  const handelOnClick = () => {
    setIsActive(!isActive);
  };
  const containerClassName = `container bg-white ${
    isActive ? "right-panel-active" : ""
  }`;

  return (
    <div className="background flex items-center justify-center">
      <div className={containerClassName}>
        {/* Sign up */}
        <div className="form-container flex items-center justify-center sign-up-container">
          <Row justify={"center"}>
            <Typography
              className="h1"
              style={{
                fontWeight: "bold",
                fontSize: "2.5rem",
                alignItems: "center",
              }}
            >
              {" "}
              Đăng Ký{" "}
            </Typography>
            <Col span={24}>
              <div className="flex justify-center w-full">
                <Form
                  style={{
                    width: "600px",
                    maxWidth: "100%",
                  }}
                  onFinish={onFinishLogup}
                >
                  <Form.Item
                    name="taiKhoan"
                    rules={[
                      {
                        required: true,
                        message: "Xin vui lòng nhập tài khoản",
                      },
                      {
                        pattern: regex.user,
                        message:
                          "Tài khoản gồm 6-12 ký tự,không khoảng cách, không ký tự đặc biệt ",
                      },
                    ]}
                  >
                    <Input
                      style={{
                        background: "#eee",
                        width: "100%",
                        padding: "8px 15px",
                      }}
                      placeholder="Tài Khoản"
                    />
                  </Form.Item>
                  <Form.Item
                    name="hoTen"
                    rules={[
                      {
                        required: true,
                        message: "Xin vui lòng nhập họ tên",
                      },
                    ]}
                  >
                    <Input
                      style={{
                        background: "#eee",
                        width: "100%",
                        padding: "8px 15px",
                      }}
                      placeholder="Họ Tên"
                    />
                  </Form.Item>
                  <Form.Item
                    name="matKhau"
                    rules={[
                      {
                        required: true,
                        message: "Xin vui lòng nhập mật khẩu",
                      },
                      {
                        pattern: regex.password,
                        message:
                          "Mật khẩu gồm 8-15 ký tự, 1 ký tự viết hoa, 1 số, 1 ký tự đặc biệt",
                      },
                    ]}
                  >
                    <Input.Password
                      style={{
                        background: "#eee",
                        width: "100%",
                        height: "auto",
                        padding: "8px 15px",
                      }}
                      placeholder="Mật Khẩu"
                    />
                  </Form.Item>
                  <Form.Item
                    name="email"
                    rules={[
                      {
                        required: true,
                        message: "Xin vui lòng nhập email",
                      },
                      {
                        type: "email",
                        message: "Vui lòng nhập đúng định dạng Email",
                      },
                    ]}
                  >
                    <Input
                      style={{
                        background: "#eee",
                        width: "100%",
                        padding: "8px 15px",
                      }}
                      placeholder="Email"
                    />
                  </Form.Item>
                  <Form.Item
                    name="soDT"
                    rules={[
                      {
                        required: true,
                        message: "Xin vui lòng nhập tài khoản",
                      },
                      {
                        pattern: regex.phone,
                        message:
                          "Xin vui lòng nhập đúng định dạng số điện thoại",
                      },
                    ]}
                  >
                    <Input
                      style={{
                        background: "#eee",
                        width: "100%",
                        padding: "8px 15px",
                      }}
                      placeholder="Số Điện Thoại"
                    />
                  </Form.Item>
                  <Form.Item initialValue="GP01" name="maNhom">
                    <Select
                      size="large"
                      options={[
                        {
                          value: "GP01",
                          label: "GP01",
                        },
                        {
                          value: "GP02",
                          label: "GP02",
                        },
                        {
                          value: "GP03",
                          label: "GP03",
                        },
                        {
                          value: "GP04",
                          label: "GP04",
                        },
                        {
                          value: "GP05",
                          label: "GP05",
                        },
                        {
                          value: "GP06",
                          label: "GP06",
                        },
                        {
                          value: "GP07",
                          label: "GP07",
                        },
                        {
                          value: "GP08",
                          label: "GP08",
                        },
                        {
                          value: "GP09",
                          label: "GP09",
                        },
                        {
                          value: "GP10",
                          label: "GP10",
                        },
                      ]}
                    />
                  </Form.Item>
                  <Form.Item
                    wrapperCol={{
                      offset: 8,
                      span: 16,
                    }}
                  >
                    <Button
                      style={{
                        width: "50%",
                        borderRadius: "20px",
                        background: "#36867b",
                        fontWeight: "500",
                        fontSize: "14px",
                        letterSpacing: "1px",
                        color: "#fff",
                        cursor: "pointer",
                      }}
                      size="large"
                      htmlType="submit"
                    >
                      <span className="text-white text-center cursor-pointer">
                        Đăng Ký
                      </span>
                    </Button>
                  </Form.Item>
                </Form>
              </div>
            </Col>
          </Row>
        </div>

        {/* Sign in */}
        <div className="form-container flex items-center justify-center sign-in-container">
          <Row justify={"center"}>
            <div className="title text-center">
              <Typography
                className="h1"
                style={{
                  fontWeight: "bold",
                  fontSize: "2.5rem",
                  alignItems: "center",
                }}
              >
                {" "}
                Đăng Nhập{" "}
              </Typography>
              <p className="title">hoặc sử dụng tài khoản đã đăng ký của bạn</p>
            </div>
            <Col span={24}>
              <Form
                style={{
                  maxWidth: "100%",
                }}
                onFinish={onFinishLogin}
                onFinishFailed={onFinishFailedLogin}
              >
                <Form.Item
                  name="taiKhoan"
                  rules={[
                    {
                      required: true,
                      message: "Xin vui lòng nhập tài khoản",
                    },
                  ]}
                >
                  <Input
                    size="large"
                    style={{ background: "#eee" }}
                    placeholder="Tài Khoản"
                  />
                </Form.Item>
                <Form.Item
                  name="matKhau"
                  rules={[
                    {
                      required: true,
                      message: "Xin vui lòng nhập mật khẩu",
                    },
                  ]}
                >
                  <Input.Password
                    size="large"
                    style={{ background: "#eee" }}
                    placeholder="Mật Khẩu"
                  />
                </Form.Item>
                <div
                  className="text-center w-full"
                  style={{ color: "#36867b", margin: "10px 0" }}
                >
                  <a>Quên mật khẩu?</a>
                </div>
                <Form.Item
                  className="w-full"
                  wrapperCol={{
                    offset: 8,
                    span: 16,
                  }}
                >
                  <Button
                    className="button"
                    style={{
                      width: "50%",
                      borderRadius: "20px",
                      background: "#36867b",
                      fontWeight: "500",
                      fontSize: "14px",
                      letterSpacing: "1px",
                      color: "#fff",
                      cursor: "pointer",
                    }}
                    size="large"
                    htmlType="submit"
                  >
                    <span className="text-white text-center  cursor-pointer">
                      ĐĂNG NHẬP
                    </span>
                  </Button>
                </Form.Item>
              </Form>
            </Col>
          </Row>
        </div>

        {/* Overlay */}
        <div className="overlay-container">
          <div className="overlay">
            <div className="overlay-panel overlay-left space-y-2">
              <p className="text-3xl font-bold h1">Chào mừng bạn đã trở lại!</p>
              <p className="title">
                Vui lòng đăng nhập để kết nối với tài khoản của bạn
              </p>
              <button
                onClick={handelOnClick}
                className="sign-in border font-bold px-10 py-2 rounded-3xl uppercase"
                style={{
                  background: "#36867b",
                  borderColor: "#fff",
                  fontSize: "12px",
                }}
              >
                Đăng Nhập
              </button>
            </div>
            <div className="overlay-panel overlay-right space-y-2">
              <p className="text-3xl font-bold h1">Xin chào!</p>
              <p className="title">
                Vui lòng nhấn đăng ký để thiết lập thông tin tài khoản của bạn!
              </p>
              <button
                onClick={handelOnClick}
                className="sign-up border rounded-3xl font-bold px-10 py-2 uppercase"
                style={{
                  background: "#36867b",
                  borderColor: "#fff",
                  fontSize: "12px",
                }}
              >
                Đăng Ký
              </button>
            </div>
          </div>
        </div>
        {/* Modal */}
        {/* modal err */}
        <Modal
          cancelButtonProps={{ style: { display: "none" } }}
          okButtonProps={{ style: { display: "none" } }}
          centered
          open={isModalOpen}
          onCancel={handleCancel}
          closeIcon={false}
          width="20vw"
          style={{
            height: "20vh",
          }}
        >
          <div className=" flex flex-col justify-center items-center space-y-5 text-center">
            <animated.div className="text-6xl text-orange-400">
              <ExclamationCircleOutlined />
            </animated.div>
            <p className="font-medium text-2xl" style={{ color: "#36867b" }}>
              {err}
            </p>
            <p>Đã xảy ra lỗi vui lòng quay lại trang chủ hoặc thử lại</p>
          </div>
        </Modal>

        {/* modal success */}
        <Modal
          cancelButtonProps={{ style: { display: "none" } }}
          okButtonProps={{ style: { display: "none" } }}
          centered
          open={isModalOpen2}
          onCancel={handleCancel2}
          closeIcon={false}
          width="20vw"
          style={{
            height: "20vh",
          }}
        >
          <div className=" flex flex-col justify-center items-center space-y-5 text-center">
            <animated.div className="text-6xl text-orange-400">
              <CheckSquareOutlined />
            </animated.div>
            <p className="font-medium text-2xl" style={{ color: "#36867b" }}>
              ĐĂNG KÝ THÀNH CÔNG
            </p>
          </div>
        </Modal>
      </div>
    </div>
  );
}
