import React from "react";
import "./AllCourses.css";

import { Desktop, Mobile, Tablet } from "../../../responsive/responsive";
import AllCouresDesktop from "./AllCoursesDesktop";
import AllCouresTablet from "./AllCoursesTablet";
import AllCouresMobile from "./AllCoursesMobile";

export default function AllCoures() {
  return (
    <>
      <Desktop>
        <AllCouresDesktop />
      </Desktop>

      <Tablet>
        <AllCouresTablet />
      </Tablet>
      
      <Mobile>
        <AllCouresMobile />
      </Mobile>
    </>
  );
}
