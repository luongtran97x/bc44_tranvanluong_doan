import React, { useEffect, useState } from "react";
import "./AllCourses.css";
import { Pagination } from "antd";
import { https } from "../../../services/config";
import { NavLink } from "react-router-dom";

export default function AllCouresDesktop() {
  const [listCourses, setListCourses] = useState([]);
  // số lượng data trong 1 trang
  const [totalCount, setTotalCount] = useState(0);
  console.log("🚀 ~ totalCount:", totalCount)
  // số trang
  const [totalPages, setTotalPages] = useState(0);
  console.log("🚀 ~ totalPages:", totalPages)
  useEffect(() => {
    fetchListCourses(1);
  }, []);

  const fetchListCourses = (page) => {
    https
      .get(
        `api/QuanLyKhoaHoc/LayDanhSachKhoaHoc_PhanTrang?page=${page}&pageSize=12&MaNhom=GP01`
      )
      .then((res) => {
        console.log("🚀 ~ res:", res)
        setListCourses(res.data);
        setTotalCount(res.data.count);
        setTotalPages(res.data.totalCount);
      
      })
      .catch((err) => {
        console.log("🚀 ~ err:", err);
      });
  };

  const handelOnChangePage = (page) => {
    fetchListCourses(page);
  };

  return (
    <div>
      <div
        className="banner"
        style={{ padding: "50px", background: "#ffd60a", color: "white" }}
      >
        <p className="text-3xl font-medium">KHÓA HỌC </p>
        <p> BẮT ĐẦU HÀNH TRÌNH NÀO!!!</p>
      </div>
      <div className="coursesContainer">
        <div className="grid grid-cols-6">
          <div className="coursesBox bgStyle1">
            <p className="text-xl font-medium">Chương trình học</p>
            <i className="fas fa-laptop text-3xl py-2"></i>
            <p className="text-lg">300</p>
          </div>
          <div className="coursesBox bgStyle2">
            <p className="text-xl font-medium">NHÀ SÁNG TẠO</p>
            <i className="fas fa-camera text-3xl py-2"></i>
            <p className="text-lg">10000</p>
          </div>
          <div className="coursesBox bgStyle3">
            <p className="text-xl font-medium">NHÀ THIẾT KẾ</p>
            <i className="fas fa-briefcase text-3xl py-2"></i>
            <p className="text-lg">400</p>
          </div>
          <div className="coursesBox bgStyle4">
            <p className="text-xl font-medium">BÀI GIẢNG</p>
            <i className="fas fa-book text-3xl py-2"></i>
            <p className="text-lg">3000</p>
          </div>
          <div className="coursesBox bgStyle5">
            <p className="text-xl font-medium">VIDEO</p>
            <i className="fas fa-play-circle text-3xl py-2"></i>
            <p className="text-lg">40000</p>
          </div>
          <div className="coursesBox bgStyle6">
            <p className="text-xl font-medium">LĨNH VỰC</p>
            <i className="fas fa-dice-d20 text-3xl py-2"></i>
            <p className="text-lg">200</p>
          </div>
        </div>
      </div>
      <div className="coursesListPage">
        <p className="text-xl font-medium">
          <i
            className="fas fa-bookmark"
            style={{ fontSize: "20px", marginRight: "5px", color: "#ed85ab" }}
          ></i>
          Danh sách khóa học
        </p>
        <div className="listItems grid grid-cols-4 pt-3 ">
          {listCourses.items &&
            listCourses.items.map((item) => {
              return (
                <div
                  key={item.maKhoaHoc}
                  className="item itemEffect pb-10 cardGlobalRes"
                >
                  <NavLink to={`/detail/${item.maKhoaHoc}`} className="cardGlobal">
                    <img
                      src={item.hinhAnh}
                      style={{ height: "185px", minHeight: "185px" }}
                      alt={item.biDanh}
                    />
                    <span className="stickerCard">{item.tenKhoaHoc}</span>
                    <div className="cardBodyGolbal">
                      <p className="font-medium">
                        {item.moTa && item.moTa.substring(0, 40) + "..."}
                      </p>
                      <div className="titleMark">
                        <div className="imgCardFooter">
                          <img
                            src="https://demo2.cybersoft.edu.vn/static/media/avatar2.bb9626e2.png"
                            className="w-full"
                            alt="hinhAnh"
                          />
                        </div>
                        <span style={{ color: "#8c8c8c", marginLeft: "2px" }}>
                          Elon Musk
                        </span>
                      </div>
                    </div>
                    <div
                      className="cardFotter flex justify-between items-center"
                      style={{
                        padding: "5px 20px",
                        borderTop: "1px solid rgba(0,0,0,.125)",
                      }}
                    >
                      <div className="">
                        <p
                          style={{
                            fontSize: "12px",
                            color: "#8c8c8c",
                            textDecorationLine: "line-through",
                          }}
                        >
                          800.000
                          <sup
                            className="relative"
                            style={{
                              fontSize: "75%",
                              lineHeight: "0",
                              verticalAlign: "baseline",
                              top: "-0.5em",
                            }}
                          >
                            đ
                          </sup>
                        </p>
                        <p style={{ fontWeight: "500", color: "#41b294" }}>
                          400.000
                          <sup
                            className="relative"
                            style={{
                              fontSize: "75%",
                              lineHeight: "0",
                              verticalAlign: "baseline",
                              top: "-0.5em",
                            }}
                          >
                            đ
                          </sup>
                        </p>
                      </div>
                      <div className="">
                        <i
                          className="fas fa-star mr-1 textStar"
                          style={{ color: "#f6ba35" }}
                        ></i>
                        <span className="textStar" style={{ color: "#f6ba35" }}>
                          4.9
                        </span>
                        <span
                          className="colorCardTitle"
                          style={{ fontSize: "12px", color: "#8c8c8c" }}
                        >
                          (7840)
                        </span>
                      </div>
                    </div>
                  </NavLink>
                </div>
              );
            })}
        </div>
        <Pagination
          onChange={handelOnChangePage}
          defaultCurrent={1}
          // số lượng data trong mảng
          total={54}
          // số item mỗi page
          pageSize={12}
        />
      </div>
    </div>
  );
}
